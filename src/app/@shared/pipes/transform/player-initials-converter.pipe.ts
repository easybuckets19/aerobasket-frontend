import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'playerInitialsConverter'
})
export class PlayerInitialsConverterPipe implements PipeTransform {

  transform(name: string): string {
    const chars: string[] = [
      name.split(' ')[0].charAt(0),
      name.split(' ')[1].charAt(0),
    ];
    return chars.join('');
  }

}
