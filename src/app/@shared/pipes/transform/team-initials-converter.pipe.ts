import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'teamInitialConverter'
})
export class TeamInitialsConverterPipe implements PipeTransform {

  transform(teamName: string): string {
    const chars: string[] = [];
    if (teamName.length <= 3)
      teamName.split('').forEach(char => chars.push(char));
    else
      teamName.split(' ').forEach(word => chars.push(word.charAt(0)));
    return chars.join('');;
  }

}
