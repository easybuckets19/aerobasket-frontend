import { Pipe, PipeTransform } from '@angular/core';
import {Team, TeamDetails} from "@data/model/team.models";

@Pipe({
  name: 'teamAvatar'
})
export class TeamAvatarPipe implements PipeTransform {
  transform(team: Team | TeamDetails ): string {
    const chars: string[] = [];
    if (team.name.length <= 3)
      team.name.split('').forEach(char => chars.push(char));
    else
      team.name.split(' ').forEach(word => chars.push(word.charAt(0)));
    return chars.join('');
  }
}
