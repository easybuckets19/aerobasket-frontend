import { Pipe, PipeTransform } from '@angular/core';
import {Team} from "@data/model/team.models";
import {Player, PlayerDetails} from "@data/model/player.models";

@Pipe({
  name: 'personNameAvatar'
})
export class PersonNameAvatarPipe implements PipeTransform {
  transform(firstName: string, lastName: string): string {
    const chars: string[] = [
      firstName.charAt(0),
      lastName.charAt(0)
    ];
    return chars.join('');
  }
}
