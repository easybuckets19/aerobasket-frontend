import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'dateValueConverter'
})
export class DateValueConverterPipe implements PipeTransform {
  transform(dateString: string): number {
    const date = new Date(dateString);
    return date.getTime();
  }
}
