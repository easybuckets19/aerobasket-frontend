import { Pipe, PipeTransform } from '@angular/core';
import {Address, Court} from "@data/model/common.models";

@Pipe({
  name: 'addressJoin'
})
export class AddressJoinPipe implements PipeTransform {
  transform(address: Address | Court): string {
    return `${address.postalCode} ${address.city}, ${address.street} ${address.streetNo}.`
  }
}
