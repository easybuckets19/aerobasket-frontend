import {Pipe, PipeTransform} from '@angular/core';

const PIPE_LOCALE = 'en';
@Pipe({
  name: 'dateArrayConverter'
})
export class DateArrayConverterPipe implements PipeTransform {
  transform(dateString: string): string[] {
    const date = new Date(dateString);
    return new Array<string>(
      date.toLocaleString(PIPE_LOCALE, { year: 'numeric' }),
      date.toLocaleString(PIPE_LOCALE, { month: 'short' }),
      date.toLocaleString(PIPE_LOCALE, { day: 'numeric' }),
    )
  }
}
