import { Pipe, PipeTransform } from '@angular/core';
import {Player} from "@data/model/player.models";
import {Team} from "@data/model/team.models";

@Pipe({
  name: 'teamFilter'
})
export class TeamFilterPipe implements PipeTransform {

  transform(value: Team[], args?: any): Team[] {
    if(!value)return null;
    if(!args)return value;

    args = args.toLowerCase();

    return value.filter(function(data){
      return JSON.stringify(data).toLowerCase().includes(args);
    });
  }

}
