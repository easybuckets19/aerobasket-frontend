import { Pipe, PipeTransform } from '@angular/core';
import {Player} from "@data/model/player.models";

@Pipe({
  name: 'playerFilter'
})
export class PlayerFilterPipe implements PipeTransform {

  transform(value: Player[], args?: any): Player[] {
    if(!value)return null;
    if(!args)return value;

    args = args.toLowerCase();

    return value.filter(function(data){
      return JSON.stringify(data).toLowerCase().includes(args);
    });
  }

}
