import { Pipe, PipeTransform } from '@angular/core';
import {Registration} from "@data/model/common.models";

@Pipe({
  name: 'registrationFilter'
})
export class RegFilterPipe implements PipeTransform {

  transform(value: Registration[], args?: number): Registration[] {
    if(!value)return null;
    if(!args)return value;

    return value.filter(function(data){
      return data.teamId === args;
    });
  }

}
