import { Pipe, PipeTransform } from '@angular/core';
import {Registration, Statistics, StatisticsDetails} from "@data/model/common.models";

@Pipe({
  name: 'statisticsFilter'
})
export class StatFilterPipe implements PipeTransform {

  transform(value: StatisticsDetails[], args?: number): StatisticsDetails[] {
    if(!value)return null;
    if(!args)return value;

    return value.filter(function(data){
      return data.teamId === args;
    });
  }

}
