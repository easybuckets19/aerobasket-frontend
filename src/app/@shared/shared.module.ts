import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule} from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AntDesignModule } from "./ant-design/ant-design.module";
import { ApexChartsModule } from "./apex-charts/apex-charts.module";
import { PerfectScrollbarModule} from "ngx-perfect-scrollbar";
import { LoadingBarRouterModule } from "@ngx-loading-bar/router";
import {TeamAvatarPipe} from "@shared/pipes/transform/team-avatar.pipe";
import {PersonNameAvatarPipe} from "@shared/pipes/transform/player-avatar.pipe";
import {AddressJoinPipe} from "@shared/pipes/transform/address-join.pipe";
import {DateValueConverterPipe} from "@shared/pipes/transform/date-converter.pipe";
import {DateArrayConverterPipe} from "@shared/pipes/transform/date-array-converter.pipe";
import {PlayerFilterPipe} from "@shared/pipes/filter/player-search.pipe";
import {TeamFilterPipe} from "@shared/pipes/filter/team-search.pipe";
import {StatFilterPipe} from "@shared/pipes/filter/stat-filter.pipe";
import {RegFilterPipe} from "@shared/pipes/filter/reg-filter.pipe";
import {StatChartWidgetComponent} from "@shared/widgets/stat-chart-widget/stat-chart-widget.component";
import {StatChartMultiWidgetComponent} from "@shared/widgets/stat-chart-multi-widget/stat-chart-multi-widget.component";
import {StatListWidgetComponent} from "@shared/widgets/list/stat-list-widget/stat-list-widget.component";
import {GameListWidgetComponent} from "@shared/widgets/list/game-list-widget/game-list-widget.component";
import {PlayerListWidgetComponent} from "@shared/widgets/list/player-list-widget/player-list-widget.component";
import {TeamListWidgetComponent} from "@shared/widgets/list/team-list-widget/team-list-widget.component";
import {PlayerInitialsConverterPipe} from "@shared/pipes/transform/player-initials-converter.pipe";
import {RegistrationWidgetComponent} from "@shared/widgets/list/registration-widget/registration-widget.component";
import {GameDetailsWidgetTestComponent} from "@shared/widgets/details/game-details-widget-test/game-details-widget-test.component";
import {PlayerDetailsWidgetTestComponent} from "@shared/widgets/details/player-details-widget-test/player-details-widget-test.component";
import {TeamDetailsWidgetTestComponent} from "@shared/widgets/details/team-details-widget-test/team-details-widget-test.component";
import {GoogleMapsModule} from "@angular/google-maps";
import { GameMapComponent } from './widgets/maps/game-map/game-map.component';
import { UserDetailsWidgetComponent } from './widgets/details/user-details-widget/user-details-widget.component';
import { PlayerRankingWidgetComponent } from './widgets/rankings/player-ranking-widget/player-ranking-widget.component';
import { TeamRankingWidgetComponent } from './widgets/rankings/team-ranking-widget/team-ranking-widget.component';
import {TeamInitialsConverterPipe} from "@shared/pipes/transform/team-initials-converter.pipe";
import { GamesListWidgetTestComponent } from './widgets/list/games-list-widget-test/games-list-widget-test.component';
import { SeasonScheduleWidgetComponent } from './widgets/season-schedule-widget/season-schedule-widget.component';
import { PlayerDialogComponent } from './widgets/dialogs/player-dialog/player-dialog.component';
import { TeamDialogComponent } from './widgets/dialogs/team-dialog/team-dialog.component';


@NgModule({
  declarations: [
    TeamAvatarPipe,
    PersonNameAvatarPipe,
    AddressJoinPipe,
    DateValueConverterPipe,
    DateArrayConverterPipe,
    PlayerInitialsConverterPipe,
    TeamInitialsConverterPipe,

    PlayerFilterPipe,
    TeamFilterPipe,
    StatFilterPipe,
    RegFilterPipe,

    StatChartWidgetComponent,
    StatChartMultiWidgetComponent,

    StatListWidgetComponent,
    GameListWidgetComponent,
    PlayerListWidgetComponent,
    TeamListWidgetComponent,
    RegistrationWidgetComponent,

    GameDetailsWidgetTestComponent,
    PlayerDetailsWidgetTestComponent,
    TeamDetailsWidgetTestComponent,
    GameMapComponent,
    UserDetailsWidgetComponent,
    PlayerRankingWidgetComponent,
    TeamRankingWidgetComponent,
    GamesListWidgetTestComponent,
    SeasonScheduleWidgetComponent,
    PlayerDialogComponent,
    TeamDialogComponent,
  ],

  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    AntDesignModule,
    ApexChartsModule,
    PerfectScrollbarModule,
    LoadingBarRouterModule,
    GoogleMapsModule,
  ],
  exports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AntDesignModule,
    ApexChartsModule,
    PerfectScrollbarModule,
    LoadingBarRouterModule,

    TeamAvatarPipe,
    PersonNameAvatarPipe,
    AddressJoinPipe,
    DateValueConverterPipe,
    DateArrayConverterPipe,
    PlayerInitialsConverterPipe,
    TeamInitialsConverterPipe,

    PlayerFilterPipe,
    TeamFilterPipe,
    StatFilterPipe,
    RegFilterPipe,

    StatChartWidgetComponent,
    StatChartMultiWidgetComponent,

    StatListWidgetComponent,
    GameListWidgetComponent,
    PlayerListWidgetComponent,
    TeamListWidgetComponent,
    RegistrationWidgetComponent,

    GameDetailsWidgetTestComponent,
    PlayerDetailsWidgetTestComponent,
    TeamDetailsWidgetTestComponent,
    GoogleMapsModule,
    GameMapComponent,
    UserDetailsWidgetComponent,
    PlayerRankingWidgetComponent,
    TeamRankingWidgetComponent,
    GamesListWidgetTestComponent,
    PlayerDialogComponent,
  ]
})
export class SharedModule { }
