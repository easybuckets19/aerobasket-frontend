import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NzLayoutModule} from "ng-zorro-antd/layout";
import {NzMenuModule} from "ng-zorro-antd/menu";
import {NzIconModule, NzIconService} from "ng-zorro-antd/icon";
import {NzCardModule} from "ng-zorro-antd/card";
import {NzTypographyModule} from "ng-zorro-antd/typography";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzMessageModule} from "ng-zorro-antd/message";
import {NzDividerModule} from "ng-zorro-antd/divider";
import {NzTagModule} from "ng-zorro-antd/tag";
import {NzStepsModule} from "ng-zorro-antd/steps";
import {NzAvatarModule} from "ng-zorro-antd/avatar";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import { GroupItemComponent } from './components/group-item/group-item.component';
import { MenuItemComponent } from './components/menu-item/menu-item.component';
import { UnwrapDirective } from "./directives/unwrap.directive";
import { RouterModule } from "@angular/router";
import {PerfectScrollbarModule} from "ngx-perfect-scrollbar";
import {NzPageHeaderModule} from "ng-zorro-antd/page-header";
import {NzDescriptionsModule} from "ng-zorro-antd/descriptions";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzStatisticModule} from "ng-zorro-antd/statistic";
import {NzListModule} from "ng-zorro-antd/list";
import {NzBadgeModule} from "ng-zorro-antd/badge";
import {NzSelectModule} from "ng-zorro-antd/select";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {NzModalModule} from "ng-zorro-antd/modal";
import {NzTabsModule} from "ng-zorro-antd/tabs";
import {NzResizableModule} from "ng-zorro-antd/resizable";
import {NzPopoverModule} from "ng-zorro-antd/popover";
import {NzDatePickerModule} from "ng-zorro-antd/date-picker";
import {NzResultModule} from "ng-zorro-antd/result";
import {NzInputNumberModule} from "ng-zorro-antd/input-number";

export interface AntSVGIcon {
  id:   string;
  path: string;
}

export const ANT_ICONS: AntSVGIcon[] = [
  { id: 'antx:dashboard', path: '<svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M19 5v2h-4V5h4M9 5v6H5V5h4m10 8v6h-4v-6h4M9 17v2H5v-2h4M21 3h-8v6h8V3zM11 3H3v10h8V3zm10 8h-8v10h8V11zm-10 4H3v6h8v-6z"/></svg>' },
  { id: 'antx:players',   path: '<svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M9 13.75c-2.34 0-7 1.17-7 3.5V19h14v-1.75c0-2.33-4.66-3.5-7-3.5zM4.34 17c.84-.58 2.87-1.25 4.66-1.25s3.82.67 4.66 1.25H4.34zM9 12c1.93 0 3.5-1.57 3.5-3.5S10.93 5 9 5 5.5 6.57 5.5 8.5 7.07 12 9 12zm0-5c.83 0 1.5.67 1.5 1.5S9.83 10 9 10s-1.5-.67-1.5-1.5S8.17 7 9 7zm7.04 6.81c1.16.84 1.96 1.96 1.96 3.44V19h4v-1.75c0-2.02-3.5-3.17-5.96-3.44zM15 12c1.93 0 3.5-1.57 3.5-3.5S16.93 5 15 5c-.54 0-1.04.13-1.5.35.63.89 1 1.98 1 3.15s-.37 2.26-1 3.15c.46.22.96.35 1.5.35z"/></svg>'},
  { id: 'antx:teams',     path: '<svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000"><g><rect fill="none" height="24" width="24" y="0"/></g><g><g><rect height="1.5" width="4" x="14" y="12"/><rect height="1.5" width="4" x="14" y="15"/><path d="M20,7h-5V4c0-1.1-0.9-2-2-2h-2C9.9,2,9,2.9,9,4v3H4C2.9,7,2,7.9,2,9v11c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V9 C22,7.9,21.1,7,20,7z M11,7V4h2v3v2h-2V7z M20,20H4V9h5c0,1.1,0.9,2,2,2h2c1.1,0,2-0.9,2-2h5V20z"/><circle cx="9" cy="13.5" r="1.5"/><path d="M11.08,16.18C10.44,15.9,9.74,15.75,9,15.75s-1.44,0.15-2.08,0.43C6.36,16.42,6,16.96,6,17.57V18h6v-0.43 C12,16.96,11.64,16.42,11.08,16.18z"/></g></g></svg>' },
  { id: 'antx:menu',      path: '<svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"/></svg>' },
  { id: 'antx:games',     path: '<svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M19 4h-1V2h-2v2H8V2H6v2H5c-1.11 0-1.99.9-1.99 2L3 20c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 16H5V10h14v10zm0-12H5V6h14v2zm-7 5h5v5h-5z"/></svg>' }
];

@NgModule({
  declarations: [
    GroupItemComponent,
    MenuItemComponent,
    UnwrapDirective,
  ],
  imports: [
    CommonModule,
    RouterModule,
    NzLayoutModule,
    NzMenuModule,
    NzIconModule,
    NzCardModule,
    NzTypographyModule,
    NzFormModule,
    NzInputModule,
    NzButtonModule,
    NzCheckboxModule,
    NzGridModule,
    NzMessageModule,
    NzDividerModule,
    NzTagModule,
    NzStepsModule,
    NzAvatarModule,
    NzDropDownModule,
    NzPageHeaderModule,
    NzDescriptionsModule,
    NzTableModule,
    NzStatisticModule,
    NzListModule,
    NzBadgeModule,
    NzSelectModule,
    NzToolTipModule,
    NzModalModule,
    NzTabsModule,
    NzPopoverModule,
    NzDatePickerModule,
    NzResultModule,
    NzInputNumberModule,
  ],
  exports: [
    NzLayoutModule,
    NzMenuModule,
    NzIconModule,
    NzCardModule,
    NzTypographyModule,
    NzFormModule,
    NzInputModule,
    NzButtonModule,
    NzCheckboxModule,
    NzGridModule,
    NzMessageModule,
    NzDividerModule,
    NzTagModule,
    NzStepsModule,
    NzAvatarModule,
    NzDropDownModule,
    NzPageHeaderModule,
    NzDescriptionsModule,
    NzTableModule,
    NzStatisticModule,
    NzListModule,
    NzBadgeModule,
    NzSelectModule,
    NzToolTipModule,
    NzModalModule,
    NzTabsModule,
    NzPopoverModule,
    NzDatePickerModule,
    NzResultModule,
    NzInputNumberModule,

    GroupItemComponent,
    MenuItemComponent,
    UnwrapDirective,

  ]
})
export class AntDesignModule {
  private _registry: NzIconService;


  constructor(registry: NzIconService) {
    this._registry = registry;
    this.registerIcons(ANT_ICONS);
  }

  private registerIcons(icons: AntSVGIcon[]): void {
    icons.forEach(icon => this.registerIcon(icon));
  }

  private registerIcon(icon: AntSVGIcon): void {
    this._registry.addIconLiteral(icon.id, icon.path);
  }

}
