import {Component, Input, OnInit} from '@angular/core';
import {AntMenuItem} from "./menu-item.types";

@Component({
  selector: 'antx-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.less']
})
export class MenuItemComponent implements OnInit {
  @Input() item: AntMenuItem;
  @Input() parent: boolean;

  constructor() { }

  ngOnInit(): void {}

}
