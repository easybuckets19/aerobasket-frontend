export interface AntMenuItem {
  label:        string;
  icon:         string;
  routerLink:   string;
  children?:    Partial<AntMenuItem>[];
}
