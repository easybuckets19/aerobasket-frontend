import {Component, Input, OnInit} from '@angular/core';
import {AntGroupItem} from "./group-item.types";

@Component({
  selector: 'antx-group-item',
  templateUrl: './group-item.component.html',
  styleUrls: ['./group-item.component.less']
})
export class GroupItemComponent implements OnInit {
  @Input() item: AntGroupItem;
  @Input() isCollapsed: boolean;

  constructor() { }

  ngOnInit(): void {}

  rootItems(group: AntGroupItem) {
    return group.items.filter(it => it.children !== undefined && it.children.length > 0)
  }

  childItems(group: AntGroupItem) {
    return group.items.filter(it => it.children === undefined || it.children.length < 1)
  }
}
