import { AntMenuItem } from "../menu-item/menu-item.types";

export interface AntGroupItem {
  title:      string;
  items:      AntMenuItem[];
}
