import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeasonScheduleWidgetComponent } from './season-schedule-widget.component';

describe('SeasonScheduleWidgetComponent', () => {
  let component: SeasonScheduleWidgetComponent;
  let fixture: ComponentFixture<SeasonScheduleWidgetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeasonScheduleWidgetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeasonScheduleWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
