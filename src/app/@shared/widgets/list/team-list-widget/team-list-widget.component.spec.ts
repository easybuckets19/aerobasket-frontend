import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamListWidgetComponent } from './team-list-widget.component';

describe('TeamListWidgetComponent', () => {
  let component: TeamListWidgetComponent;
  let fixture: ComponentFixture<TeamListWidgetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeamListWidgetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamListWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
