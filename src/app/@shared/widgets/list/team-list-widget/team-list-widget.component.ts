import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Team} from "@data/model/team.models";
import {NzTableFilterFn, NzTableFilterList, NzTableSortFn, NzTableSortOrder} from "ng-zorro-antd/table";

interface TeamColumn {
  name:             string;
  sortOrder:        NzTableSortOrder        | null;
  sortFn:           NzTableSortFn<Team>     | null;
  filterValues:     NzTableFilterList;
  filterFn:         NzTableFilterFn<Team>   | null;
  filterMultiple:   boolean;
  sortDirections:   NzTableSortOrder[];
}

@Component({
  selector: 'team-list-widget',
  templateUrl: './team-list-widget.component.html',
  styleUrls: ['./team-list-widget.component.less']
})
export class TeamListWidgetComponent implements OnInit {
  @Input()  teams:      Team[];
  @Input()  title:      string;
  @Input()  subTitle:   string;
  @Output() onTeamSelected = new EventEmitter<Team>();

  displayedColumns: TeamColumn[] = [];

  ngOnInit(): void {
    this.displayedColumns = [
      {
        name: 'ID',
        sortOrder: null,
        sortFn: null,
        sortDirections: [null],
        filterMultiple: false,
        filterValues: [],
        filterFn: null,
      },
      {
        name: 'Team',
        sortOrder: null,
        sortFn: null,
        sortDirections: [null],
        filterMultiple: false,
        filterValues: [],
        filterFn: null,
      },
      {
        name: 'Registration Date',
        sortOrder: null,
        sortFn: null,
        sortDirections: [null],
        filterMultiple: false,
        filterValues: [],
        filterFn: null,
      },
      {
        name: 'Owner',
        sortOrder: null,
        sortFn: null,
        sortDirections: [null],
        filterMultiple: false,
        filterValues: [],
        filterFn: null,
      },
      {
        name: 'Status',
        sortOrder: null,
        sortFn: null,
        sortDirections: [null],
        filterMultiple: false,
        filterValues: [
          { text: 'Active',   value: true,  },
          { text: 'Inactive', value: false, }
        ],
        filterFn: (isActive: boolean, item: Team) => isActive === item.active
      },
    ];
  }

  constructor(){}

  emitTeamSelectedEvent(selectedTeam: Team): void {
    this.onTeamSelected.emit(selectedTeam);
  }
}
