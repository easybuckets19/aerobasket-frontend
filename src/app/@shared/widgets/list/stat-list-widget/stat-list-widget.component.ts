import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {StatisticsDetails} from "@data/model/common.models";
import {PersonNameAvatarPipe} from "@shared/pipes/transform/player-avatar.pipe";

type DisplayedStatColumnNames = 'player' | '3pt' | '2pt' | '1pt' | 'extra' | 'fouls' | 'actions' ;

@Component({
  selector: 'stat-list-widget',
  templateUrl: './stat-list-widget.component.html',
  styleUrls: ['./stat-list-widget.component.less']
})
export class StatListWidgetComponent implements OnInit {
  @Input()  statistics:           StatisticsDetails[];
  @Input()  showHeader:           boolean;
  @Input()  teamName:             string;
  @Input()  teamScore:            number;
  @Input()  displayedColumnNames: DisplayedStatColumnNames[];

  @Output() onStatSelectedEvent   = new EventEmitter<StatisticsDetails>();
  @Output() onStatIncreasedEvent  = new EventEmitter<StatisticsDetails>();
  @Output() onStatDecreasedEvent  = new EventEmitter<StatisticsDetails>();

  ngOnInit(): void {}

  ngAfterViewInit(): void {}

  constructor() {}

  public getAvatarText(statData: StatisticsDetails): string {
    return new PersonNameAvatarPipe().transform(statData.playerFirstName, statData.playerLastName);
  }

  public emitStatSelectedEvent(statData: StatisticsDetails) {
    this.onStatSelectedEvent.emit(statData);
  }

  public emitStatIncreasedEvent(statData: StatisticsDetails) {
    this.onStatIncreasedEvent.emit(statData);
  }

  public emitStatDecreasedEvent(statData: StatisticsDetails) {
    this.onStatDecreasedEvent.emit(statData);
  }

}
