import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatListWidgetComponent } from './stat-list-widget.component';

describe('StatListWidgetComponent', () => {
  let component: StatListWidgetComponent;
  let fixture: ComponentFixture<StatListWidgetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatListWidgetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatListWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
