import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameListWidgetComponent } from './game-list-widget.component';

describe('GameListWidgetComponent', () => {
  let component: GameListWidgetComponent;
  let fixture: ComponentFixture<GameListWidgetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GameListWidgetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameListWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
