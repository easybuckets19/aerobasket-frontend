import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

import {Game} from "@data/model/game.models";


@Component({
  selector: 'game-list-widget',
  templateUrl: './game-list-widget.component.html',
  styleUrls: ['./game-list-widget.component.less']
})
export class GameListWidgetComponent implements OnInit {
  @Input()  games:    Game[];
  @Input()  title:    string;
  @Input()  subTitle: string;
  @Output() onGameSelectedEvent = new EventEmitter<Game>();

  constructor() { }

  ngOnInit(): void {
  }

  emitGameSelectedEvent(selectedGame: Game): void {
    this.onGameSelectedEvent.emit(selectedGame);
  }

}
