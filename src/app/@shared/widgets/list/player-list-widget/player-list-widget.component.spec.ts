import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerListWidgetComponent } from './player-list-widget.component';

describe('PlayerListWidgetComponent', () => {
  let component: PlayerListWidgetComponent;
  let fixture: ComponentFixture<PlayerListWidgetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlayerListWidgetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerListWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
