import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Player} from "@data/model/player.models";
import {PersonNameAvatarPipe} from "@shared/pipes/transform/player-avatar.pipe";
import {NzTableFilterFn, NzTableFilterList, NzTableSortFn, NzTableSortOrder} from "ng-zorro-antd/table";

interface PlayerColumn {
  name:             string;
  sortOrder:        NzTableSortOrder        | null;
  sortFn:           NzTableSortFn<Player>   | null;
  filterValues:     NzTableFilterList;
  filterFn:         NzTableFilterFn<Player> | null;
  filterMultiple:   boolean;
  sortDirections:   NzTableSortOrder[];
}

@Component({
  selector: 'player-list-widget',
  templateUrl: './player-list-widget.component.html',
  styleUrls: ['./player-list-widget.component.less']
})
export class PlayerListWidgetComponent implements OnInit {
  @Input()  players:            Player[];
  @Input()  title:              string;
  @Input()  subTitle:           string;
  @Output() onPlayerSelected    = new EventEmitter<Player>();

  displayedColumns: PlayerColumn[] = [];

  ngOnInit(): void {
    this.displayedColumns = [
      {
        name: 'ID',
        sortOrder: null,
        sortFn: null,
        sortDirections: [null],
        filterMultiple: false,
        filterValues: [],
        filterFn: null,
      },
      {
        name: 'Name',
        sortOrder: null,
        sortFn: null,
        sortDirections: [null],
        filterMultiple: false,
        filterValues: [],
        filterFn: null,
      },
      {
        name: 'Birth Date',
        sortOrder: null,
        sortFn: null,
        sortDirections: [null],
        filterMultiple: false,
        filterValues: [],
        filterFn: null,
      },
      {
        name: 'Height',
        sortOrder: null,
        sortFn: null,
        sortDirections: [null],
        filterMultiple: false,
        filterValues: [],
        filterFn: null,
      },
      {
        name: 'Weight',
        sortOrder: null,
        sortFn: null,
        sortDirections: [null],
        filterMultiple: false,
        filterValues: [],
        filterFn: null,
      },
      {
        name: 'Status',
        sortOrder: null,
        sortFn: null,
        sortDirections: [null],
        filterMultiple: false,
        filterValues: [
          { text: 'Active',   value: true,  },
          { text: 'Inactive', value: false, }
        ],
        filterFn: (isActive: boolean, item: Player) => isActive === item.active
      },
    ];
  }

  constructor(){}

  emitPlayerSelectedEvent(selectedPlayer: Player): void {
    this.onPlayerSelected.emit(selectedPlayer);
  }

}
