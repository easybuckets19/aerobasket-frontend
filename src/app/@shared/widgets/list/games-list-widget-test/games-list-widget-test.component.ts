import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Game} from "@data/model/game.models";

type DisplayedColumnNames = | 'id' | 'teams' | 'scores' | 'date' | 'time';

@Component({
  selector: 'games-list-widget-test',
  templateUrl: './games-list-widget-test.component.html',
  styleUrls: ['./games-list-widget-test.component.less']
})
export class GamesListWidgetTestComponent implements OnInit {
  @Input()  games:                Game[];
  @Input()  title:                string;
  @Input()  subTitle:             string;
  @Input()  showHeader:           boolean;
  @Input()  displayedColumnNames: DisplayedColumnNames[];

  ngOnInit(): void {}




}
