import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GamesListWidgetTestComponent } from './games-list-widget-test.component';

describe('GamesListWidgetTestComponent', () => {
  let component: GamesListWidgetTestComponent;
  let fixture: ComponentFixture<GamesListWidgetTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GamesListWidgetTestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GamesListWidgetTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
