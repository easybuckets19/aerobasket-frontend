import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Registration, StatisticsDetails} from "@data/model/common.models";
import {BehaviorSubject, Observable} from "rxjs";

type DisplayedStatColumnNames = 'id' | 'name' | 'position' | 'birth' | 'jersey' | 'action';

@Component({
  selector: 'registration-widget',
  templateUrl: './registration-widget.component.html',
  styleUrls: ['./registration-widget.component.less']
})
export class RegistrationWidgetComponent implements OnInit, OnDestroy {
  @Input()  registrations:          Registration[];
  @Input()  selectedRegistrations:  Registration[];
  @Input()  showHeader:             boolean;
  @Input()  teamName:               string;
  @Input()  teamScore:              number;
  @Input()  displayedColumnNames:   DisplayedStatColumnNames[];

  @Output() onRegistrationSelectedEvent     = new EventEmitter<Registration>();
  @Output() onAllRegistrationSelectedEvent  = new EventEmitter<boolean>();

  private checkedSubject = new BehaviorSubject<boolean>(false);
  checked$: Observable<boolean> =this.checkedSubject.asObservable();

  ngOnInit(): void {
    this.onAllRegistrationSelectedEvent.subscribe(next => {
      this.changeChecked();
    });
  }

  ngOnDestroy(): void {
  }

  constructor() { }

  public emitRegistrationSelectedEvent(regData: Registration) {
    this.onRegistrationSelectedEvent.emit(regData);
  }

  public emitAllRegistrationSelectedEvent () {
    this.onAllRegistrationSelectedEvent.emit(!this.checkedSubject.value);
  }

  private changeChecked(): void {
    const currentValue = this.checkedSubject.value;
    this.checkedSubject.next(!currentValue);
  }


}
