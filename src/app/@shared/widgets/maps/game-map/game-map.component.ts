import {Component, EventEmitter, Input, OnInit, Output, QueryList, ViewChildren} from '@angular/core';
import {GameDetails} from "@data/model/game.models";
import {MapInfoWindow, MapMarker} from "@angular/google-maps";
import {UserDetails} from "@data/model/user.model";

@Component({
  selector: 'game-map-widget',
  templateUrl: './game-map.component.html',
  styleUrls: ['./game-map.component.less']
})
export class GameMapComponent implements OnInit {
  @Input()  gameDetails:            GameDetails;
  @Input()  availableReferees:      UserDetails[];
  @Input()  mainTitle:              string;
  @Input()  intoTitle:              string

  @Output() onRefereeAssignedEvent = new EventEmitter<UserDetails>();

  @ViewChildren(MapInfoWindow)  windows: QueryList<MapInfoWindow>;
  @ViewChildren(MapMarker)      markers: QueryList<MapMarker>;

  mapOptions:     google.maps.MapOptions;
  circleOptions:  google.maps.CircleOptions;
  originOptions:  google.maps.MarkerOptions;
  markerOptions:  google.maps.MarkerOptions;

  constructor() { }

  ngOnInit(): void {
    this.mapOptions     = this.initMapOptions();
    this.circleOptions  = this.initCircleOptions();
    this.originOptions  = this.initOriginOptions();
    this.markerOptions  = this.initMarkerOptions();
  }

  initOriginOptions(): google.maps.MarkerOptions {
    return {
      label: {
        text: "\ue1b3", // codepoint from https://fonts.google.com/icons
        fontFamily: "Material Icons",
        color: "#ffffff",
        fontSize: "12px",
      }
    }
  }

  private initMapOptions(): google.maps.MapOptions {
    return {
      zoom: 8,
      mapTypeControl: false,
      zoomControl: true,
      zoomControlOptions: {
        position: google.maps.ControlPosition.LEFT_CENTER,
      },
      scaleControl:       true,
      streetViewControl:  false,
      fullscreenControl:  true,
    }
  }

  private initCircleOptions(): google.maps.CircleOptions {
    return  {
      fillColor:    '#11A1FD',
      strokeColor:  '#11A1FD'
    }
  }

  openInfoWindow(infoIndex: number) {
    const matchedWindow: MapInfoWindow = this.windows.get(infoIndex);
    const matchedMarker: MapMarker = this.markers.get(infoIndex + 1);
    matchedWindow.open(matchedMarker);
  }

  emitRefereeAssignedEvent(refereeData: UserDetails) {
    this.windows.forEach(w => w.close());
    this.onRefereeAssignedEvent.emit(refereeData);
  }

  private initMarkerOptions() {
    return {
      label: {
        text: "\ue7fd", // codepoint from https://fonts.google.com/icons
        fontFamily: "Material Icons",
        color: "#ffffff",
        fontSize: "12px",
      }
    }
  }
}
