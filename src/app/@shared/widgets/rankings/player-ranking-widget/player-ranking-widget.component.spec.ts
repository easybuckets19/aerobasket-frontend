import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerRankingWidgetComponent } from './player-ranking-widget.component';

describe('PlayerRankingWidgetComponent', () => {
  let component: PlayerRankingWidgetComponent;
  let fixture: ComponentFixture<PlayerRankingWidgetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlayerRankingWidgetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerRankingWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
