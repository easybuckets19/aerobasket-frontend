import {Component, Input, OnInit} from '@angular/core';
import {PlayerRanking} from "@data/model/player.models";

type DisplayedStatColumnNames = '#' | 'id' | 'name' | 'total' | 'avg';

@Component({
  selector: 'player-ranking-widget',
  templateUrl: './player-ranking-widget.component.html',
  styleUrls: ['./player-ranking-widget.component.less']
})
export class PlayerRankingWidgetComponent implements OnInit {
  @Input()  rankings:             PlayerRanking[];
  @Input()  title:                string;
  @Input()  showHeader:           boolean;
  @Input()  displayedColumnNames: DisplayedStatColumnNames[];

  constructor() { }

  ngOnInit(): void {

  }

}
