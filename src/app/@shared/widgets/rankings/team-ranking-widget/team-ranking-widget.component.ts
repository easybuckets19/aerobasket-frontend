import {Component, Input, OnInit} from '@angular/core';
import {TeamRanking} from "@data/model/team.models";

type DisplayedStatColumnNames = '#' | 'id' | 'name' | 'games' | 'total' | 'avg';

@Component({
  selector: 'team-ranking-widget',
  templateUrl: './team-ranking-widget.component.html',
  styleUrls: ['./team-ranking-widget.component.less']
})
export class TeamRankingWidgetComponent implements OnInit {
  @Input()  rankings:             TeamRanking[];
  @Input()  title:                string;
  @Input()  showHeader:           boolean;
  @Input()  displayedColumnNames: DisplayedStatColumnNames[];

  constructor() { }

  ngOnInit(): void {
  }

}
