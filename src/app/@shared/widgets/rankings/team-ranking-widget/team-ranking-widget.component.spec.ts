import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamRankingWidgetComponent } from './team-ranking-widget.component';

describe('TeamRankingWidgetComponent', () => {
  let component: TeamRankingWidgetComponent;
  let fixture: ComponentFixture<TeamRankingWidgetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeamRankingWidgetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamRankingWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
