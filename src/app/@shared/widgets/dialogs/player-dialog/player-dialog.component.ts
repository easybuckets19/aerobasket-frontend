import {Component, OnDestroy, OnInit} from '@angular/core';
import {BehaviorSubject, Observable, Subscription} from "rxjs";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NzMessageService} from "ng-zorro-antd/message";
import {NzModalRef} from "ng-zorro-antd/modal";
import {PlayerService} from "@data/services/player.service";
import {Player} from "@data/model/player.models";

export interface PlayerDialogResult {
  type: 'success' | 'error',
  data?: Player,
}

@Component({
  selector: 'app-player-dialog',
  templateUrl: './player-dialog.component.html',
  styleUrls: ['./player-dialog.component.less']
})
export class PlayerDialogComponent implements OnInit, OnDestroy {
  isLoadingSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  isLoading$: Observable<boolean> = this.isLoadingSubject.asObservable();
  form: FormGroup;
  playerDialogSub: Subscription;

  constructor(
    private modalRef:       NzModalRef<PlayerDialogComponent, PlayerDialogResult>,
    private playerService:  PlayerService,
    private messageService: NzMessageService,
    private builder:        FormBuilder,
   ) {}

  ngOnInit(): void {
    this.form = this.builder.group({
      name:       [null, [Validators.required]],
      birth:      [null, [Validators.required]],
      height:     [null, [Validators.min(0)]],
      weight:     [null, [Validators.min(0)]]
    });
  }


  get birth(): Date {
    return this.form.get('birth').value;
  }

  get height(): number {
    return this.form.get('height').value;
  }

  get weight(): number {
    return this.form.get('weight').value;
  }

  get firstName(): string {
    return this.form.get('name').value.toString().split(' ')[0];
  }

  get lastName(): string {
    return this.form.get('name').value.toString().split(' ')[1];
  }


  onCancel(): void {
    this.modalRef.triggerCancel();
  }

  onSave(): void {
    this.changeLoadingStatus();
    setTimeout(() => {
      this.playerDialogSub = this.playerService
        .createPlayer(
          {
            firstName:  this.firstName,
            lastName:   this.lastName,
            birthDate:  this.birth,
            height:     this.height,
            weight:     this.weight
          }
        )
        .subscribe(
          result => {
            this.messageService.success("Successful player registration!")
            this.modalRef.destroy({ type: 'success',  data: result });
          },
          error => {
            this.messageService.error("Unsuccessful player registration!");
            this.modalRef.destroy({ type: 'error' });
          },
        );
    }, 1250);
  }

  public changeLoadingStatus(value?: boolean) : void {
    const currentValue = this.isLoadingSubject.value;
    this.isLoadingSubject.next(value ?? !currentValue);
  }

  ngOnDestroy(): void {
    this.playerDialogSub.unsubscribe();
  }
}
