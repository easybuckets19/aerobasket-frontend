import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {BehaviorSubject, Observable, of, Subject, Subscription} from "rxjs";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NzModalRef} from "ng-zorro-antd/modal";
import {PlayerService} from "@data/services/player.service";
import {NzMessageService} from "ng-zorro-antd/message";
import {Team} from "@data/model/team.models";
import {debounceTime, delay, distinctUntilChanged, switchMap} from "rxjs/operators";
import {MapsService} from "@data/services/maps.service";
import {Address, AddressLookup} from "@data/model/common.models";

export interface TeamDialogResult {
  type: 'success' | 'error',
  data?: Team,
}

@Component({
  selector: 'app-team-dialog',
  templateUrl: './team-dialog.component.html',
  styleUrls: ['./team-dialog.component.less']
})
export class TeamDialogComponent implements OnInit {
  private inputChangedSubject = new Subject<string>()
  private fetchingDataSubject = new Subject<boolean>();
  private isLoadingSubject    = new BehaviorSubject<boolean>(false);

  isLoading$: Observable<boolean>     = this.isLoadingSubject.asObservable();
  fetchingData$: Observable<boolean>  = this.fetchingDataSubject.asObservable();
  form: FormGroup; addressList: AddressLookup[] = [];

  constructor(
    private modalRef:       NzModalRef<TeamDialogComponent, TeamDialogResult>,
    private playerService:  PlayerService,
    private messageService: NzMessageService,
    private mapsService:    MapsService,
    private builder:        FormBuilder,
  ) {}

  ngOnInit(): void {
    this.form = this.builder.group({
      name:         [null,    [Validators.required]],
      coaches:      [null,    [Validators.required]],
      address:      [null,    [Validators.required]],
      matchAddress: [false,   [Validators.required]],
    });
    this.form.get('coaches').valueChanges.subscribe(next => console.log(next));
    this.inputChangedSubject.pipe(
      debounceTime(10000),
      distinctUntilChanged(),
      switchMap(value => {
        if (value.length > 0) {
          return this.mapsService.fetchPlaces(value);
        }
        return of([]);
      })
    ).subscribe(places  => {
        this.addressList = places;
        this.fetchingDataSubject.next(false);
    });
  }


  get birth(): Date {
    return this.form.get('birth').value;
  }

  get height(): number {
    return this.form.get('height').value;
  }

  get weight(): number {
    return this.form.get('weight').value;
  }

  get firstName(): string {
    return this.form.get('name').value.toString().split(' ')[0];
  }

  get lastName(): string {
    return this.form.get('name').value.toString().split(' ')[1];
  }


  onCancel(): void {
    this.modalRef.triggerCancel();
  }

  onSave(): void {
    this.changeLoadingStatus();
    setTimeout(() => {
      this.playerService
        .createPlayer(
          {
            firstName:  this.firstName,
            lastName:   this.lastName,
            birthDate:  this.birth,
            height:     this.height,
            weight:     this.weight
          }
        )
        .subscribe(
          result => {
            this.messageService.success("Successful player registration!")
            this.modalRef.destroy({ type: 'success' });
          },
          error => {
            this.messageService.error("Unsuccessful player registration!");
            this.modalRef.destroy({ type: 'error' });
          },
        );
    }, 1250);
  }

  public changeLoadingStatus(value?: boolean) : void {
    const currentValue = this.isLoadingSubject.value;
    this.isLoadingSubject.next(value ?? !currentValue);
  }

  ngOnDestroy(): void {}

  onSearch($event: string) {
    this.addressList = [];
    this.fetchingDataSubject.next(true);
    this.inputChangedSubject.next($event);
  }

}
