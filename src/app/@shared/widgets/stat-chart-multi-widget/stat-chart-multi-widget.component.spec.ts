import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatChartMultiWidgetComponent } from './stat-chart-multi-widget.component';

describe('StatChartMultiWidgetComponent', () => {
  let component: StatChartMultiWidgetComponent;
  let fixture: ComponentFixture<StatChartMultiWidgetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatChartMultiWidgetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatChartMultiWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
