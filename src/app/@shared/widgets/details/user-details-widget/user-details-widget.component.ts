import {Component, Input, OnInit} from '@angular/core';
import {UserDetails} from "@data/model/user.model";

type DisplayedFieldNames = 'id' | 'name' | 'address' | 'distance' | 'gameCount';

@Component({
  selector: 'user-details-widget',
  templateUrl: './user-details-widget.component.html',
  styleUrls: ['./user-details-widget.component.less']
})
export class UserDetailsWidgetComponent implements OnInit {
  @Input()  details:                UserDetails;
  @Input()  mainTitle:              string;
  @Input()  intoTitle:              string
  @Input()  displayedColumnNames:   DisplayedFieldNames[];

  constructor() { }

  ngOnInit(): void {
  }

}
