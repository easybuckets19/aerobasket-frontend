import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDetailsWidgetComponent } from './user-details-widget.component';

describe('UserDetailsWidgetComponent', () => {
  let component: UserDetailsWidgetComponent;
  let fixture: ComponentFixture<UserDetailsWidgetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserDetailsWidgetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDetailsWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
