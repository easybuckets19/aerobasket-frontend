import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerDetailsWidgetTestComponent } from './player-details-widget-test.component';

describe('PlayerDetailsWidgetTestComponent', () => {
  let component: PlayerDetailsWidgetTestComponent;
  let fixture: ComponentFixture<PlayerDetailsWidgetTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlayerDetailsWidgetTestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerDetailsWidgetTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
