import {Component, Input, OnInit} from '@angular/core';
import {PlayerDetails} from "@data/model/player.models";
import {Registration} from "@data/model/common.models";

type DisplayedFieldNames = 'id' | 'name' | 'personal' | 'team' | 'position' | 'jersey' | 'birth' | 'status';

@Component({
  selector: 'player-details-widget-test',
  templateUrl: './player-details-widget-test.component.html',
  styleUrls: ['./player-details-widget-test.component.less']
})
export class PlayerDetailsWidgetTestComponent implements OnInit {
  @Input()  details:                PlayerDetails;
  @Input()  registration:           Registration;
  @Input()  mainTitle:              string;
  @Input()  intoTitle:              string
  @Input()  displayedColumnNames:   DisplayedFieldNames[];

  constructor() { }

  ngOnInit(): void {
  }

}
