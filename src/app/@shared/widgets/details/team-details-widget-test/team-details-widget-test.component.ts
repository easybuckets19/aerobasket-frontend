import {Component, Input, OnInit} from '@angular/core';
import {Registration} from "@data/model/common.models";
import {TeamDetails} from "@data/model/team.models";

type DisplayedFieldNames = 'id' | 'name' | 'coaches' | 'address' | 'owner' | 'registration' | 'status' ;

@Component({
  selector: 'team-details-widget-test',
  templateUrl: './team-details-widget-test.component.html',
  styleUrls: ['./team-details-widget-test.component.less']
})
export class TeamDetailsWidgetTestComponent implements OnInit {
  @Input()  details:                TeamDetails;
  @Input()  registrations:          Registration[];
  @Input()  mainTitle:              string;
  @Input()  intoTitle:              string
  @Input()  displayedColumnNames:   DisplayedFieldNames[];

  constructor() { }

  ngOnInit(): void {
  }

}
