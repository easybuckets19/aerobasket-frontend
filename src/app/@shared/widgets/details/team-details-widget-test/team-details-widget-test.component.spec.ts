import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamDetailsWidgetTestComponent } from './team-details-widget-test.component';

describe('TeamDetailsWidgetTestComponent', () => {
  let component: TeamDetailsWidgetTestComponent;
  let fixture: ComponentFixture<TeamDetailsWidgetTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeamDetailsWidgetTestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamDetailsWidgetTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
