import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {GameDetails} from "@data/model/game.models";

type DisplayedFieldNames = 'id' | 'round' | 'location' | 'teamHome' | 'teamAway' | 'dateTime'  | 'status' ;

@Component({
  selector: 'game-details-widget-test',
  templateUrl: './game-details-widget-test.component.html',
  styleUrls: ['./game-details-widget-test.component.less']
})
export class GameDetailsWidgetTestComponent implements OnInit, OnDestroy {
  @Input()  details:                GameDetails;
  @Input()  mainTitle:              string;
  @Input()  intoTitle:              string
  @Input()  displayedColumnNames:   DisplayedFieldNames[];

  ngOnInit(): void {

  }

  ngOnDestroy(): void {

  }

  constructor() { }

}
