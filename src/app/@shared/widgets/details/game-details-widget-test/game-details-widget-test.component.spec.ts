import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameDetailsWidgetTestComponent } from './game-details-widget-test.component';

describe('GameDetailsWidgetTestComponent', () => {
  let component: GameDetailsWidgetTestComponent;
  let fixture: ComponentFixture<GameDetailsWidgetTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GameDetailsWidgetTestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameDetailsWidgetTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
