import {Component, Input, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {
  ApexAxisChartSeries,
  ApexChart,
  ApexDataLabels, ApexFill, ApexMarkers,
  ApexPlotOptions,
  ApexStroke,
  ApexTitleSubtitle, ApexXAxis, ApexYAxis, ChartComponent
} from "ng-apexcharts";
import {StatisticsDetails} from "@data/model/common.models";

export const STAT_DESCRIPTION_MAP = new Map([
  ["pt1T",        ['ft taken',   'Free throws taken'  ]],
  ["pt2T",        ['2pt taken',   '2 pointers taken'  ]],
  ["pt3T",        ['3pt taken',   '3 pointers taken'  ]],
  ["pt1M",        ['ft',          'Free throws made'  ]],
  ["pt2M",        ['2pt made',    '2 pointers made'   ]],
  ["pt3M",        ['3pt made',    '3 pointers made'   ]],
  ["assists",     ['assists',     'Total assists'     ]],
  ["rebounds",    ['rebounds',    'Total rebounds'    ]],
  ["blocks",      ['blocks',      'Total blocks'      ]],
  ["fouls",       ['fouls',       'Total fouls'       ]],
  ['steals',      ['steals',      'Total steals'      ]],
  ['turnovers',   ['turnovers',   'Total steals'      ]],
]);

export const STAT_DISPLAYED_FIELDS = ['pt3M', 'pt1M', 'pt2M', 'assists', 'rebounds', 'steals', 'turnovers'];

export type ChartOptions = {
  series:       ApexAxisChartSeries;
  chart:        ApexChart;
  title:        ApexTitleSubtitle;
  stroke:       ApexStroke;
  dataLabels:   ApexDataLabels;
  tooltip:      any;
  plotOptions:  ApexPlotOptions;
  fill:         ApexFill;
  colors:       string[];
  yaxis:        ApexYAxis;
  markers:      ApexMarkers;
  xAxis:        ApexXAxis;
};

@Component({
  selector: 'stat-chart-widget',
  templateUrl: './stat-chart-widget.component.html',
  styleUrls: ['./stat-chart-widget.component.less']
})
export class StatChartWidgetComponent implements OnInit {
  @Input()  title:  string;
  @Input()  data:   Partial<StatisticsDetails>;

  chartOptions: Partial<ChartOptions>;

  @ViewChild("chart", { static: false })
  private chart: ChartComponent;

  ngOnInit(): void {
    this.chartOptions = {
      series: [
        {
          name: "Amount",
          data: this.getData()
        }
      ],
      chart: {
        height: 350,
        type: "radar",
        fontFamily: 'Inter',
        toolbar: {
          show: false
        }
      },
      dataLabels: {
        enabled: true
      },
      plotOptions: {
        radar: {
          size: 140,
          polygons: {
            fill: {
              colors: ["#f8f8f8", "#fff"]
            }
          }
        }
      },
      colors: ["#11A1FD"],
      markers: {
        size: 8,
        colors: ["#fff"],
        strokeColors: ["#11A1FD"],
        strokeWidth: 1
      },
      tooltip: {
        y: {
          formatter: function(statData: number) {
            return statData;
          },
          title: {
          },
        },
        x: {
          formatter: function(statLabel: string) {
            const   entries       = Array.from(STAT_DESCRIPTION_MAP.entries())
            const   matchedEntry  = entries.find( ([k, v]) => k === statLabel);
            return  matchedEntry[1][1];
          }
        }
      },
      xAxis: {
        categories: this.getCategories()
      },
      yaxis: {
        tickAmount: 6,
      }
    };
  }

  ngOnChanges(changes: SimpleChanges) {
    if(this.chartOptions) {
      this.chartOptions.series = [{
        data: this.getData()
      }];
    }
  }

  private getCategories() {
    const entries = Object.entries(this.data).filter(([key]) => STAT_DISPLAYED_FIELDS.includes(key));
    return entries.map(([k,_]) => { return k })
  }

  private getData() {
    const entries = Object.entries(this.data).filter(([key]) => STAT_DISPLAYED_FIELDS.includes(key));
    return entries.map(([_,v]) => { return v as number })
  }

}
