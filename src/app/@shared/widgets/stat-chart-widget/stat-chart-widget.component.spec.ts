import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatChartWidgetComponent } from './stat-chart-widget.component';

describe('StatChartWidgetComponent', () => {
  let component: StatChartWidgetComponent;
  let fixture: ComponentFixture<StatChartWidgetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatChartWidgetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatChartWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
