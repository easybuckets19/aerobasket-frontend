import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainComponent } from './layouts/main/main.component';
import { AuthComponent } from './layouts/auth/auth.component';
import { SharedModule } from "@shared/shared.module";
import { CoreModule } from "@core/core.module";
import { DataModule } from "@data/data.module";
import { ToolbarComponent } from "./layouts/main/toolbar/toolbar.component";
import { SidebarComponent } from './layouts/main/sidebar/sidebar.component';
import { RedirectComponent } from './layouts/redirect/redirect.component';

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    AuthComponent,
    ToolbarComponent,
    SidebarComponent,
    RedirectComponent,
  ],
  imports: [
    // Routing-related modules      - used to define paths and redirects.
    AppRoutingModule,

    // Angular-related modules      - required infrastructure for the framework.
    BrowserModule,  BrowserAnimationsModule,

    // Application-related modules  - additional modules added after scaffold.
    SharedModule, CoreModule, DataModule,
  ],
  providers: [
    { provide: NZ_I18N, useValue: en_US },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
