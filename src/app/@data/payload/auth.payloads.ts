export interface LoginDTO {
  username: string;
  password: string;
}

export interface RegistrationDTO {
  username:   string;
  password:   string;
  email:      string;
  firstName:  string;
  lastName:   string;
}
