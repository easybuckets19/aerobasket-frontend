export interface Player {
  id:             number;
  name:           string;
  active:         boolean;
  team:           string;
  weight:         number;
  height:         number;
  birthDate:      string;
}

export interface PlayerDetails {
  id:             number;
  name:           string;
  active:         boolean;
  team:           string;
  weight:         number;
  height:         number;
  birthDate:      string;
}

export interface PlayerCreateDTO {
  firstName:      string;
  lastName:       string;
  weight:         number;
  height:         number;
  birthDate:      Date;
}

export interface PlayerUpdateDTO {
  firstName:      string;
  lastName:       string;
  weight:         number;
  height:         number;
  birthDate:      Date;
}


export interface PlayerRanking {
  id: number;
  name: string;
  totalPoints: number;
  totalRebounds: number;
  totalAssists: number;
  totalSteals: number;
  totalTurnovers: number;
  avgPoints: number;
  avgRebounds: number;
  avgAssists: number;
  avgSteals: number;
  avgTurnovers: number;
}

