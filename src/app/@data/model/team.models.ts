import {Address} from "@data/model/common.models";
import {User} from "@data/model/user.model";

export interface Team {
  id:               number;
  name:             string;
  active:           boolean;
  registeredAt:     string;
  address:          Address;
  owner:            User;
}

export interface TeamDetails {
  id:               number;
  name:             string;
  active:           boolean;
  primaryCoach:     string;
  secondaryCoach:   string;
  registeredAt:     string;
  address:          Address;
  owner:            User;
}

export interface TeamRanking {
  id: number;
  name: string;
  gamesPlayed: number;
  gamesWon: number;
  gamesLost: number;
  totalPoints: number;
  totalRebounds: number;
  totalAssists: number;
  totalSteals: number;
  totalTurnovers: number;
  avgPoints: number;
  avgRebounds: number;
  avgAssists: number;
  avgSteals: number;
  avgTurnovers: number;
}
