export interface Registration {
  id: number;
  playerId: number;
  playerName: string;
  playerBirth: string;
  teamId: number;
  teamName: string;
  fromDate: string;
  toDate: string;
  position: string;
  jerseyNumber: number;
  seasonId: number;
}

export interface Statistics {
  id: number;
  pt3M: number;
  pt3T: number;
  pt2M: number;
  pt2T: number;
  pt1T: number;
  pt1M: number;
  assists: number;
  rebounds: number;
  steals: number;
  turnovers: number;
}


export interface StatisticsDetails {
  id: number;
  teamName: string;
  teamId: number;
  playerFirstName: string;
  playerLastName: string;
  playerId: number;
  pt3M: number;
  pt3T: number;
  pt2M: number;
  pt2T: number;
  pt1T: number;
  pt1M: number;
  assists: number;
  rebounds: number;
  steals: number;
  turnovers: number;
  fouls: number
}

export interface AddressLookup {
  mapsId:       string
  result:       string;
}

// TODO: Refactor
export interface Address {
  mapsId:       string
  postalCode:   string;
  country:      string;
  city:         string;
  street:       string;
  streetNo:     string;
  lat:          number;
  lng:          number;
}
// TODO: Refactor
export interface Court {
  mapsId:       string;
  postalCode:   string;
  country:      string;
  city:         string;
  street:       string;
  streetNo:     string;
  lat:          number;
  lng:          number;
}

// TODO: Refactor
export interface Referee {
  mapsId:       string;
  postalCode:   string;
  country:      string;
  city:         string;
  street:       string;
  streetNo:     string;
  lat:          number;
  lng:          number;
  prefDest:     number;
}



