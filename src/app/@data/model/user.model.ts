import {Address} from "@data/model/common.models";

export interface User {
  id:         number;
  username:   string;
  email:      string;
  name:       string;
  roles:      Role[];
}

export interface UserDetails {
  id:               number;
  username:         string;
  email:            string;
  name:             string;
  roles:            Role[];
  address:          Address;
  status:           string;
  prefDistance:     number;
  actualDistance:   number;
}

export interface Role {
  id:       number;
  name:     UserRole;
}

export type UserRole = 'ROLE_ADMIN' | 'ROLE_OWNER' | 'ROLE_ASSISTANT' | 'ROLE_REFEREE';

