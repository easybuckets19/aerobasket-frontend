import { Court } from "@data/model/common.models";

export interface Game {
  id:                   number;
  roundNumber:          number;
  scheduledStartTime:   string;
  scheduledFinishTime:  string;
  teamHome:             string;
  teamHomeScore:        number;
  teamAway:             string;
  teamAwayScore:        number;
}


export interface GameDetails {
  id:                   number;
  roundNumber:          number;
  scheduledStartTime:   string;
  scheduledFinishTime:  string;
  teamHomeId:           number;
  teamHome:             string;
  teamHomeScore:        number;
  teamAwayId:           number;
  teamAway:             string;
  teamAwayScore:        number;
  status:               GameStatus;
  location:             Court;
}

export type GameStatus = 'STATUS_SCHEDULED' | 'STATUS_IN_PROGRESS' | 'STATUS_OVERVIEW' | 'STATUS_FINISHED'
