import { Injectable } from '@angular/core';
import { ApiService } from '@data/services/api.service';
import { Observable } from 'rxjs';
import {Player} from "@data/model/player.models";
import {Registration, Statistics, StatisticsDetails} from "@data/model/common.models";
import {Game, GameDetails} from "@data/model/game.models";
import {UserDetails} from "@data/model/user.model";

export type GameStatus = 'STATUS_SCHEDULED' | 'STATUS_IN_PROGRESS' | 'STATUS_OVERVIEW' | 'STATUS_FINISHED';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  constructor(private api: ApiService) {}

  fetchGames(): Observable<Game[]> {
    return this.api.sendHttpRequest('GET', `/games`);
  }

  fetchUpcomingGames(): Observable<Game[]> {
    return this.api.sendHttpRequest('GET', `/games/upcoming`);
  }

  fetchRecentGames(): Observable<Game[]> {
    return this.api.sendHttpRequest('GET', `/games/recent`);
  }

  fetchGameDetails(id: number): Observable<GameDetails> {
    return this.api.sendHttpRequest('GET', `/games/${id}`);
  }

  fetchAvailableReferees(id: number): Observable<UserDetails[]> {
    return this.api.sendHttpRequest('GET', `/games/${id}/referees`);
  }


  updateGameStatus(id: number, status: GameStatus): Observable<GameDetails> {
    return this.api.sendHttpRequest('PATCH', `/games/${id}`,{}, { updateStatus: status } );
  }

  updateGameStat(id: number, statId: number): Observable<StatisticsDetails> {
    return this.api.sendHttpRequest('PUT', `/games/${id}/stats/${statId}`);
  }

  updateReferees(id: number, userId): Observable<GameDetails> {
    return this.api.sendHttpRequest('PUT', `/games/${id}/referees/${userId}`);
  }

  fetchGamePlayers(id: number): Observable<Registration[]> {
    return this.api.sendHttpRequest('GET', `/games/${id}/players`);
  }

  fetchGameStats(id: number): Observable<StatisticsDetails[]> {
    return this.api.sendHttpRequest('GET', `/games/${id}/stats`);
  }



}
