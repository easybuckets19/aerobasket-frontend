import { Injectable } from '@angular/core';
import { ApiService } from '@data/services/api.service';
import { Observable } from 'rxjs';
import {Player, PlayerDetails, PlayerCreateDTO, PlayerRanking, PlayerUpdateDTO} from "@data/model/player.models";
import {Registration, Statistics} from "@data/model/common.models";
import {Game} from "@data/model/game.models";

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  constructor(private api: ApiService) {}

  getPlayers(): Observable<Player[]> {
    return this.api.sendHttpRequest('GET', `/players`);
  }

  getPlayerDetails(id: number): Observable<PlayerDetails> {
    return this.api.sendHttpRequest('GET', `/players/${id}`);
  }

  createPlayer(playerCreateDTO: PlayerCreateDTO): Observable<Player> {
    return this.api.sendHttpRequest('POST',`/players`, playerCreateDTO);
  }

  updatePlayer(id: number, playerUpdateDTO: PlayerUpdateDTO): Observable<Player> {
    return this.api.sendHttpRequest('POST',`/players/${id}`);
  }

  getRegistration(id: number): Observable<Registration> {
    return this.api.sendHttpRequest('GET', `/players/${id}/registration`);
  }

  getStats(id: number): Observable<Statistics[]> {
    return this.api.sendHttpRequest('GET', `/players/${id}/stats`);
  }

  getGames(id: number): Observable<Game[]> {
    return this.api.sendHttpRequest('GET', `/players/${id}/games`);
  }

  getRankings(): Observable<PlayerRanking[]> {
    return this.api.sendHttpRequest('GET', `/players/rankings`);
  }

}
