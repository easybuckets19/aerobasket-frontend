import { Injectable } from '@angular/core';
import { ApiService } from '@data/services/api.service';
import { Observable } from 'rxjs';
import {Team, TeamDetails, TeamRanking} from "@data/model/team.models";
import {Statistics} from "@data/model/common.models";
import {Game} from "@data/model/game.models";

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  constructor(private api: ApiService) {}

  fetchTeams(): Observable<Team[]> {
    return this.api.sendHttpRequest('GET', `/teams`);
  }

  fetchRankings(): Observable<TeamRanking[]> {
    return this.api.sendHttpRequest('GET', `/teams/rankings`);
  }

  fetchTeamDetails(id: number): Observable<TeamDetails> {
    return this.api.sendHttpRequest('GET', `/teams/${id}`);
  }

  fetchRecentStats(id: number): Observable<Statistics[]> {
    return this.api.sendHttpRequest('GET', `/teams/${id}/stats`);
  }

  fetchRecentGames(id: number): Observable<Game[]> {
    return this.api.sendHttpRequest('GET', `/teams/${id}/games`);
  }

}
