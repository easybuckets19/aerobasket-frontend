import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";

export type HttpMethod = 'GET' | 'POST' | 'DELETE' | 'PATCH' | 'PUT';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private readonly BASE_URL     = 'http://localhost:8080';
  private readonly HTTP_HEADERS = new HttpHeaders({'Content-Type': 'application/json'});
  private readonly HTTP_PARAMS  = new HttpParams();

  constructor(private httpClient: HttpClient) {}

  sendHttpRequest<T>(method: HttpMethod, path: string, requestBody: any = {}, requestParams: any = {}): Observable<T> {
    console.log(`Call: ${this.BASE_URL.concat(path)}, Body: ${JSON.stringify(requestBody)}`);
    return this.httpClient.request<T>(method, this.BASE_URL.concat(path), {
      body:     requestBody,
      headers:  this.HTTP_HEADERS,
      params:   this.HTTP_PARAMS.appendAll(requestParams)
    });
  }

  get baseURL(): string {
    return this.BASE_URL;
  }

}
