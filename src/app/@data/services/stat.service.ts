import { Injectable } from '@angular/core';
import { ApiService } from '@data/services/api.service';
import { Observable } from 'rxjs';
import {Player} from "@data/model/player.models";
import {Registration, Statistics, StatisticsDetails} from "@data/model/common.models";
import {Game, GameDetails} from "@data/model/game.models";
import {UserDetails} from "@data/model/user.model";
import {SignInDTO} from "@data/payload/sign-in.dto";


export class CreateStatDTO {
  private registrationId: number;
  private pt3M: number = 0;
  private pt3T: number = 0;
  private pt2M: number = 0;
  private pt2T: number = 0;
  private pt1T: number = 0;
  private pt1M: number = 0;
  private assists: number = 0;
  private rebounds: number = 0;
  private steals: number = 0;
  private turnovers: number = 0;
  private fouls: number = 0;

  constructor(registrationId: number) {
    this.registrationId = registrationId;
  }

}

export interface UpdateStatDTO {
  pt3M: number;
  pt3T: number;
  pt2M: number;
  pt2T: number;
  pt1T: number;
  pt1M: number;
  assists: number;
  rebounds: number;
  steals: number;
  turnovers: number;
  fouls: number;
}

@Injectable({
  providedIn: 'root'
})
export class StatService {


  constructor(private api: ApiService) {}

  updateStat(id: number, payload: UpdateStatDTO): Observable<StatisticsDetails> {
    return this.api.sendHttpRequest('PUT', `/stats/${id}`, payload );
  }

  createStat(payload: CreateStatDTO): Observable<Statistics> {
    return this.api.sendHttpRequest('POST', `/stats`, payload );
  }

}
