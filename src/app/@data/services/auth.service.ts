import { Injectable } from '@angular/core';
import {ApiService} from "@data/services/api.service";
import {Observable} from "rxjs";
import { LoginDTO, RegistrationDTO } from "@data/payload/auth.payloads";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private api: ApiService) {}

  login(payload: LoginDTO): Observable<any> {
    return this.api.sendHttpRequest('POST', '/auth/login', payload);
  }

  register(payload: RegistrationDTO): Observable<any> {
    return this.api.sendHttpRequest('POST', '/auth/register', payload);
  }

}
