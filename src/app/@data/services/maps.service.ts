import {Injectable} from "@angular/core";
import {ApiService} from "@data/services/api.service";
import {Observable} from "rxjs";
import {Address} from "@data/model/common.models";

@Injectable({
  providedIn: 'root'
})
export class MapsService {

  constructor(private api: ApiService) {}

  fetchPlaces(query: string): Observable<Address[]> {
    return this.api.sendHttpRequest('GET', `/maps/placeAutoComplete`, {} , { query: query });
  }

}
