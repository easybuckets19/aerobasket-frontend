import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, CanActivateChild, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {GameService} from "@data/services/game.service";
import {map} from "rxjs/operators";
import {GameDetails} from "@data/model/game.models";

@Injectable({
  providedIn: 'root'
})
export class GameStatusGuard implements CanActivateChild {

  constructor(private gameService: GameService) {
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.gameService
      .fetchGameDetails(this.currentPath(childRoute))
      .pipe(map(data => {
        const details: GameDetails  = data;
        const statusArray: string[] = childRoute.data.gameStatus;
        return details.status === statusArray[0];
      }));
  }

  private currentPath(route: ActivatedRouteSnapshot): number {
    return Number.parseInt(route.parent.paramMap.get('id'));
  }


}
