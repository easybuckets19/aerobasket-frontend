import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import {forkJoin} from "rxjs";
import {GameService} from "@data/services/game.service";

@Injectable()
export class GameDetailsResolver implements Resolve<any> {
  constructor(
    private router: Router,
    private gameService: GameService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    const gameDetails$   = this.gameService.fetchGameDetails(this.currentPath(route));
    const gameStats$     = this.gameService.fetchGameStats(this.currentPath(route));

    return forkJoin([
      gameDetails$,
      gameStats$,
    ]);
  }

  private currentPath(route: ActivatedRouteSnapshot): number {
    return Number.parseInt(route.paramMap.get('id'));
  }
}
