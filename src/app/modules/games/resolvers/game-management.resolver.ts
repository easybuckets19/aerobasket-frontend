import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {GameService} from "@data/services/game.service";
import {forkJoin} from "rxjs";

@Injectable()
export class GameManagementResolver implements Resolve<any> {
  constructor(
    private router: Router,
    private gameService: GameService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    const gameDetails$        = this.gameService.fetchGameDetails(this.currentPath(route));
    const gameRegistrations$  = this.gameService.fetchGamePlayers(this.currentPath(route));
    const gameStatistics$     = this.gameService.fetchGameStats(this.currentPath(route));
    const gameReferees$       = this.gameService.fetchAvailableReferees((this.currentPath(route)));

    return forkJoin([
      gameDetails$,
      gameRegistrations$,
      gameStatistics$,
      gameReferees$,
    ]);
  }

  private currentPath(route: ActivatedRouteSnapshot): number {
    return Number.parseInt(route.paramMap.get('id'));
  }
}
