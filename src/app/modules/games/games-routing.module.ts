import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListViewComponent } from "@modules/games/views/list-view/list-view.component";
import { DetailsViewComponent } from "@modules/games/views/details-view/details-view.component";
import { GameDetailsResolver } from "@modules/games/resolvers/game-details.resolver";
import { GameManagementResolver } from "@modules/games/resolvers/game-management.resolver";
import { GameStatusGuard } from "@modules/games/resolvers/game-status.guard";
import  {ManageViewComponent } from "@modules/games/views/manage-view/manage-view.component";

const routes: Routes = [
  {
    path: '',
    component:  ListViewComponent,
  },
  {
    path: ':id',
    component:  DetailsViewComponent,
    resolve: {
      gameDetails: GameDetailsResolver,
    }
  },
  {
    path:               ':id/management',
    component:          ManageViewComponent,
    canActivateChild:   [GameStatusGuard],
    resolve:            { gameDetails: GameManagementResolver },

  },

];

@NgModule({
  imports:    [RouterModule.forChild(routes)],
  exports:    [RouterModule],
  providers:  [GameDetailsResolver, GameManagementResolver ],
})
export class GamesRoutingModule { }
