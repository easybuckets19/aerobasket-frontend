import { NgModule } from '@angular/core';
import { ListViewComponent } from './views/list-view/list-view.component';
import { DetailsViewComponent } from './views/details-view/details-view.component';
import { GamesRoutingModule } from "@modules/games/games-routing.module";
import { SharedModule } from "@shared/shared.module";
import { VerificationComponent } from './components/verification/verification.component';
import { StatisticsComponent } from './components/statistics/statistics.component';
import { OverviewComponent } from './components/overview/overview.component';
import { ManageViewComponent } from "@modules/games/views/manage-view/manage-view.component";

@NgModule({
  declarations: [
    ListViewComponent,
    DetailsViewComponent,
    ManageViewComponent,
    VerificationComponent,
    StatisticsComponent,
    OverviewComponent,
  ],
  imports: [
    GamesRoutingModule,
    SharedModule,
  ]
})
export class GamesModule { }
