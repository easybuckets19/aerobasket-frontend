import {Component, OnDestroy, OnInit} from '@angular/core';
import { GameDetails } from "@data/model/game.models";
import { StatisticsDetails } from "@data/model/common.models";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'game-details-view',
  templateUrl: './details-view.component.html',
  styleUrls: ['./details-view.component.less']
})
export class DetailsViewComponent implements OnInit, OnDestroy {
  gameDetails:  GameDetails;
  gameStats:    StatisticsDetails[];
  homeStats:    StatisticsDetails[];
  awayStats:    StatisticsDetails[];

  // TODO: Might change to BehaviourSubject for better readability.
  selectedStat: StatisticsDetails;

  ngOnInit(): void {
    this.initData();
  }

  ngOnDestroy(): void {}

  private initData() {
    this.gameDetails  = this.route.snapshot.data['gameDetails'][0] as GameDetails;
    this.gameStats    = this.route.snapshot.data['gameDetails'][1] as StatisticsDetails[];
    this.selectedStat = this.gameStats[0];
  }

  constructor(private route: ActivatedRoute){}

  handleStatSelectedEvent(details: StatisticsDetails) {
    this.selectedStat = details;
  }

  get responsiveGutterClasses(): any[] {
    return [
      { xs: 8, sm: 16, md: 24, lg: 24, xl: 24, xxl: 24 },
      { xs: 8, sm: 16, md: 24, lg: 24, xl: 24, xxl: 24 }
    ];
  }

}
