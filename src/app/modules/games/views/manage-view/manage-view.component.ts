import {Component, Inject, OnInit} from '@angular/core';
import {GameDetails} from "@data/model/game.models";
import {concat, forkJoin, from, Observable, Subscription, timer} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {Registration, StatisticsDetails} from "@data/model/common.models";
import {concatAll, switchMap, tap} from "rxjs/operators";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {GameService} from "@data/services/game.service";
import {UserDetails} from "@data/model/user.model";
import {CreateStatDTO, StatService} from "@data/services/stat.service";

@Component({
  selector: 'game-manage-view',
  templateUrl: './manage-view.component.html',
  styleUrls: ['./manage-view.component.less']
})
export class ManageViewComponent implements OnInit {
  gameDetails:            GameDetails;
  gameStart:              Date;
  gameFinish:             Date;
  gameStatistics:         StatisticsDetails[];
  gameReferees:           UserDetails[];
  gameRegistrations:      Registration[];

  activePlayers:          Registration[] = [];
  activeReferees:         UserDetails[] = [];
  activeStats:            StatisticsDetails[] = [];

  currentPhase:           number;
  availablePhases:        number[] = [];


  ngOnInit(): void {
    this.initData();
    this.initPhases();
  }

  ngOnDestroy(): void {}

  constructor(
    private route:        ActivatedRoute,
    private modalService: NzModalService,
    private gameService:  GameService,
    private statService:  StatService,
  ){}

  private initData() {
    this.gameDetails        = this.route.snapshot.data['gameDetails'][0] as GameDetails;
    this.gameRegistrations  = this.route.snapshot.data['gameDetails'][1] as Registration[];
    this.gameStatistics     = this.route.snapshot.data['gameDetails'][2] as StatisticsDetails[];
    this.gameReferees       = this.route.snapshot.data['gameDetails'][3] as UserDetails[];
  }

  private initPhases() {
    this.gameStart     = new Date(2021, 11, 5);
    this.gameFinish    = new Date(2021, 11, 5);
    this.currentPhase  = this.extractCurrentPhase();
    this.availablePhases.push(this.currentPhase);
  }

  private extractCurrentPhase(): number {
    switch (this.gameDetails.status){
      case 'STATUS_SCHEDULED':
        return  0;
      case 'STATUS_IN_PROGRESS':
        return  1;
      case 'STATUS_OVERVIEW':
        return  2;
      default:
        return -1;
    }
  }

  private createPhase1Modal(): NzModalRef {
    return this.modalService.confirm({
      nzTitle:      'This modal is a verification to enter PHASE 1',
      nzContent:    'After clicking the save button, the game will start.',
      nzOkText:     'Save',
      nzOnOk: () =>  this.executePhase1Actions(this.gameDetails, this.activePlayers)
    });
  }

  private createPhase2Modal(): NzModalRef {
    return this.modalService.confirm({
      nzTitle:      'This modal is a verification to enter PHASE 2',
      nzContent:    'After clicking the save button, the game will finish.',
      nzOkText:     'Save',
      nzOnOk: () =>  this.executePhase2Actions(this.gameDetails, this.activeStats)
    });
  }

  public handlePhaseChangedEvent(nextStep?: number) {
    if(this.currentPhase == 0 && nextStep == 1){
       this.createPhase1Modal()
         .afterClose
         .subscribe(() => this.currentPhase = nextStep);
    }
    if(this.currentPhase == 1 && nextStep == 2){
      this.createPhase2Modal()
        .afterClose
        .subscribe(() => this.currentPhase = nextStep);
    }
  }

  public handleActivePlayersChangedEvent(players: Registration[]) {
    this.activePlayers = [...players];
  }

  public handleActiveRefereesChanged(referees: UserDetails[]) {
    this.activeReferees = [...referees];
  }


  public handleActiveStatsChanged(statistics: StatisticsDetails[]) {
    this.activeStats = [...statistics];
  }

  private executePhase1Actions(gameDetails: GameDetails, activePlayers: Registration[]): Subscription {
    const gameDetails$  = this.gameService
      .updateGameStatus(gameDetails.id, 'STATUS_IN_PROGRESS')
      .pipe(tap(updatedDetails => this.gameDetails = updatedDetails));
    const gameStats$ = forkJoin(
      activePlayers
        .map(p => this.statService.createStat(new CreateStatDTO(p.id)).pipe(
            switchMap(createdStat => this.gameService.updateGameStat(gameDetails.id, createdStat.id))),
      )).pipe(tap( updatedStats => this.gameStatistics = updatedStats));
    return concat(gameDetails$, gameStats$).subscribe();
  }

  private executePhase2Actions(gameDetails: GameDetails, activeStats: StatisticsDetails[]): Subscription {
    const gameDetails$  = this.gameService
      .updateGameStatus(gameDetails.id, 'STATUS_SCHEDULED')
      .pipe(tap(updatedDetails => this.gameDetails = updatedDetails));
    const gameStats$ = forkJoin(
      activeStats
        .map(stat => this.statService.updateStat(stat.id, {...stat}))
      ).pipe(tap( updatedStats => this.gameStatistics = updatedStats));
    return concat(gameStats$).subscribe();
  }

}
