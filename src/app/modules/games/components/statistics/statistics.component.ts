import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {GameDetails} from "@data/model/game.models";
import {Registration, StatisticsDetails} from "@data/model/common.models";
import {Subscription, timer} from "rxjs";
import {NzSelectComponent} from "ng-zorro-antd/select";

export const STAT_DESCRIPTION_MAP = new Map([
  ["pt1T",        'Free throws taken' ],
  ["pt2T",        '2 pointers taken'  ],
  ["pt3T",        '3 pointers taken'  ],
  ["pt1M",        'Free throws made'  ],
  ["pt2M",        '2 pointers made'   ],
  ["pt3M",        '3 pointers made'   ],
  ["assists",     'Assists'           ],
  ["rebounds",    'Rebounds'          ],
  ["blocks",      'Blocks'            ],
  ["fouls",       'Fouls'             ],
  ['steals',      'Steals'            ],
  ['turnovers',   'Turnovers'         ],
]);

@Component({
  selector: 'game-manage-view-stats',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.less']
})
export class StatisticsComponent implements OnInit, OnDestroy {
  statisticsMap       = STAT_DESCRIPTION_MAP;
  statisticsSelected  = 'pt1M';

  @Input()  details:                GameDetails;
  @Input()  finishTime:             Date;
  @Input()  statistics:             StatisticsDetails[];
  @Output() onActiveStatsChanged    = new EventEmitter<StatisticsDetails[]>();
  @Output() onGameFinished          = new EventEmitter<void>();

  @ViewChild(NzSelectComponent)     selectComponent: NzSelectComponent;

  private timerSubscription: Subscription;

  ngOnInit(): void {
    this.initScore();
    this.initTimer();
  }

  ngOnDestroy() {
    this.timerSubscription.unsubscribe();
  }

  get responsiveGutterClasses(): any[] {
    return [
      { xs: 8, sm: 16, md: 24, lg: 24, xl: 24, xxl: 24 },
      { xs: 8, sm: 16, md: 24, lg: 24, xl: 24, xxl: 24 }
    ];
  }

  protected initScore() {
    this.details.teamHomeScore = 0;
    this.details.teamAwayScore = 0;
  }

  protected initTimer() {
    this.timerSubscription = timer(this.finishTime).subscribe(next => {
      this.onGameFinished.emit();
    })
  }

  constructor() {}

  public handleStatDetailsChangedEvent(statDetails: StatisticsDetails, increase: boolean) {
    const playerStat = this.statistics.find(s => s.id == statDetails.id);
    if (increase){
      this.updateStat(playerStat, increase);
      this.updateScore(playerStat, increase);
    }
    else {
      this.updateStat(playerStat, increase);
      this.updateScore(playerStat, increase);
    }
    this.onActiveStatsChanged.emit(this.statistics);
  }

  public onSelectedStatChanged(selectionKey: string) {
    this.statisticsSelected = selectionKey;
  }

  private updateScore(playerStat: StatisticsDetails, increase: boolean) {
      let changeAmount = 0;
      switch (this.statisticsSelected){
          case 'pt1M':
              changeAmount = 1;
              break;
          case 'pt2M':
              changeAmount = 2;
              break
          case 'pt3M':
              changeAmount = 3;
              break;
      }
      this.changeScore(playerStat, increase, changeAmount);
  }

  private changeScore(playerStat: StatisticsDetails, increase: boolean, amount: number) {
    if (playerStat.teamId === this.details.teamHomeId) {
      this.details.teamHomeScore += increase ? amount : (this.details.teamHomeScore > 0 ? amount * - 1 : 0)
    } else {
      this.details.teamAwayScore += increase ? amount : (this.details.teamAwayScore > 0 ? amount * - 1 : 0)
    }
  }

  private updateStat(playerStat: StatisticsDetails, increase: boolean) {
      const keysChanged: string[] = [ this.statisticsSelected ];
      switch (this.statisticsSelected){
        case 'pt1M':
          keysChanged.push('pt1T')
          break;
        case 'pt2M':
          keysChanged.push('pt2T')
          break
        case 'pt3M':
          keysChanged.push('pt3T')
          break;
      }
      this.changeStat(playerStat, increase, keysChanged);
  }

  private changeStat(playerStat: StatisticsDetails, increase: boolean, modifiedKeys: string[]) {
    Object.entries(playerStat).forEach(([key, value]) => {
      if (modifiedKeys.includes(key)){
        const changeAmount = increase ? 1 : (value > 0 ? -1 : 0);
        playerStat[key]    = playerStat[key] + changeAmount;
      }
    });
  }

}
