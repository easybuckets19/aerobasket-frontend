import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {GameDetails} from "@data/model/game.models";
import {Registration} from "@data/model/common.models";
import {ActivatedRoute} from "@angular/router";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {GameService} from "@data/services/game.service";
import {forkJoin, timer} from "rxjs";
import {UserDetails} from "@data/model/user.model";
import {tap} from "rxjs/operators";
import {NzMessageService} from "ng-zorro-antd/message";

@Component({
  selector: 'game-manage-view-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.less']
})
export class OverviewComponent implements OnInit, OnDestroy {
  @Input()  details:                GameDetails;
  @Input()  startTime:              Date;
  @Input()  registrations:          Registration[];
  @Input()  referees:               UserDetails[];

  @Output() onActivePlayersChanged  = new EventEmitter<Registration[]>();
  @Output() onActiveRefereesChanged = new EventEmitter<UserDetails[]>();
  @Output() onGameStarted           = new EventEmitter<void>();

  selectedRegistrations:          Registration[]  = [];
  selectedReferees:               UserDetails[]   = [];

  ngOnInit(): void {
    timer(this.startTime).subscribe(next => {
      this.onGameStarted.emit();
    })
    this.onActiveRefereesChanged.subscribe(next => {
      if (next.length == 2) {
        this.createSaveModal();
      }
    })
  }

  ngOnDestroy(): void {}

  constructor(
    private route: ActivatedRoute,
    private gameService: GameService,
    private messageService: NzMessageService,
    private modalService: NzModalService){}

  handlePlayerSelectedEvent(registration: Registration) {
    if (this.selectedRegistrations.includes(registration)){
      this.selectedRegistrations.splice(this.selectedRegistrations.findIndex(p => p.id === registration.playerId), 1);
    }
    else {
      this.selectedRegistrations.push(registration);
    }
    console.log(`Handle single selection 💦 [length: ${this.selectedRegistrations.length}]`)
    this.onActivePlayersChanged.emit(this.selectedRegistrations);
  }

  handleRefereeAssignedEvent(details: UserDetails) {
    if (!this.selectedReferees.includes(details)){
      this.selectedReferees.push(details);
    }
    console.log(`Handle referees assigned 💦 [length: ${this.selectedReferees.length}]`)
    this.onActiveRefereesChanged.emit(this.selectedReferees);
  }

  handleAllSelectedEvent(selectAll: boolean, teamId: number) {
    if (selectAll) {
      this.selectedRegistrations.push(...this.registrations.filter(p => !this.selectedRegistrations.includes(p) && p.teamId === teamId));
    } else {
      this.selectedRegistrations = [];
    }
    console.log(`Handle multi selection 💦 [length: ${this.selectedRegistrations.length}]`)
    this.onActivePlayersChanged.emit(this.selectedRegistrations);
  }

  private createSaveModal(): NzModalRef {
    return this.modalService.confirm({
      nzTitle:      'Referees confirmation',
      nzContent:    'After clicking the save button, the referees will be assigned to the game and notified about the game details.',
      nzOkText:     'Save',
      nzOnOk: () => forkJoin([
        this.gameService.updateReferees(this.details.id, this.referees[0].id),
        this.gameService.updateReferees(this.details.id, this.referees[1].id),
      ]).subscribe(
        success => {
          this.messageService.success("Referees assigned to the game")
        }, error => {
          this.messageService.error(error.error.message);
        }
      ),
      nzOnCancel: () => {
        this.selectedReferees = [];
      }
    });
  }

  getAvailableReferees() {
    return this.referees.filter( r => !this.selectedReferees.includes(r));
  }
}
