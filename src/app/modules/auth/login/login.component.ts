import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {BehaviorSubject, Observable, of} from "rxjs";
import {AuthStoreService} from "@core/services/auth-store.service";
import {delay, finalize} from "rxjs/operators";
import {Router} from "@angular/router";
import {EventBusService} from "@core/services/event-bus.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {
  private maskedSubject                 = new BehaviorSubject<boolean>(true);
  public  masked$: Observable<boolean>  = this.maskedSubject.asObservable();
  public  form: FormGroup;

  constructor(private authStore: AuthStoreService, private builder: FormBuilder) {}

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.form = this.builder.group({
      username: [null, [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(16)]
      ],
      password: [null, [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(16)]
      ],
      remember: [false]
    });
  }

  public changeMasked() {
    this.maskedSubject.next(!this.maskedSubject.value);
  }

  public handleLogin(): void {
    this.authStore.login({
      username: this.username,
      password: this.password
    }).subscribe();
  }

  get responsiveGutterClasses(): any[] {
    return [
      { xs: 8, sm: 16, md: 24, lg: 32, xl: 32, xxl: 32 },
      { xs: 8, sm: 16, md: 24, lg: 32, xl: 32, xxl: 32 }
    ];
  }

  get username(): string  {
    return this.form.get('username')!.value;
  }

  get password(): string {
    return this.form.get('password')!.value;
  }

}
