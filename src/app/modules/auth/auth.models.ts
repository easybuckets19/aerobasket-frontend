import { User } from "@data/model/user.model";
import { SignUpDTO }  from "@data/payload/sign-up.dto";
import { SignInDTO }  from "@data/payload/sign-in.dto";

export interface AuthStateModel {
  user: User | null;
  accessToken: string | null;
  loading: boolean;
}

export class Reset {
  static readonly type = '[Auth] Reset state';
  constructor() {}
}

export class SignIn {
  static readonly type = '[Auth] Signing In';
  constructor(public payload: SignInDTO) {}
}

export class SignUp {
  static readonly type = '[Auth] Signing Up';
  constructor(public payload: SignUpDTO) {}
}

export class Bypass {
  static readonly type = '[Auth] Silent Auth';
  constructor() {}
}

