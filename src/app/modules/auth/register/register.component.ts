import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BehaviorSubject, Observable } from "rxjs";
import {Router} from "@angular/router";
import {AuthStoreService} from "@core/services/auth-store.service";
import {Role} from "@data/model/user.model";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.less']
})
export class RegisterComponent implements OnInit {
  private maskedSubject                 = new BehaviorSubject<boolean>(true);
  public  masked$: Observable<boolean>  = this.maskedSubject.asObservable();
  public  form: FormGroup;
  rolesList: Role[] = [
    { id: 2, name: 'ROLE_ASSISTANT' },
    { id: 3, name: 'ROLE_OWNER'     },
    { id: 3, name: 'ROLE_REFEREE'   },
  ];

  constructor(
    private router:         Router,
    private authStore:      AuthStoreService,
    private builder:        FormBuilder
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.form = this.builder.group({
      username: [null,
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(16)
        ]
      ],
      name: [null,
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(999)
        ]
      ],
      email: [null, [
        Validators.required,
        Validators.email]
      ],
      password: [null,
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(16)
        ]
      ],
      role: [null,
        [
          Validators.required,
        ]
      ]
    });
  }

  get responsiveGutterClasses(): any[] {
    return [
      { xs: 8, sm: 16, md: 24, lg: 32, xl: 32, xxl: 32 },
      { xs: 8, sm: 16, md: 24, lg: 32, xl: 32, xxl: 32 }
    ];
  }

  get lastName(): string  {
    return this.form.get('name')!.value.split(' ')[1];
  }

  get firstName(): string  {
    return this.form.get('name')!.value.split(' ')[0];
  }

  get email(): string  {
    return this.form.get('email')!.value;
  }

  get role(): Role[]  {
    return this.form.get('role')!.value;
  }

  get username(): string  {
    return this.form.get('username')!.value;
  }

  get password(): string {
    return this.form.get('password')!.value;
  }

  public changeMasked() {
    this.maskedSubject.next(!this.maskedSubject.value);
  }

  public handleRegistration(): void {
    this.authStore.register({
      username:   this.username,
      password:   this.password,
      email:      this.email,
      firstName:  this.firstName,
      lastName:   this.lastName,
    })
      .subscribe(
        success => this.handleSuccess(),
        error => this.handleError()
      );
  }

  private handleSuccess(): void {
    this.router.navigate(['/auth/login']);
  }

  private handleError(): void {
    this.authStore.logout();
    this.form.reset();
  }

}
