import {Component, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import {Subscription} from "rxjs";
import {Player} from "@data/model/player.models";
import {FormBuilder, FormGroup} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {debounceTime, distinctUntilChanged} from "rxjs/operators";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {PlayerDialogComponent, PlayerDialogResult} from "@shared/widgets/dialogs/player-dialog/player-dialog.component";

@Component({
  selector: 'player-list-view',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.less']
})
export class PlayersComponent implements OnInit, OnDestroy {
  private _form:              FormGroup;
  private _valueChangesSub:   Subscription;

  players: Player[];
  playerSearchCriteria: string = '';

  ngOnInit(): void {
    this.initData();
    this.initSubscriptions();
  }

  ngOnDestroy(): void {
    this._valueChangesSub.unsubscribe();
  }

  private initData() {
    this.players = this.route.snapshot.data['players'];
    this._form = this.builder.group({
      search: [ this.playerSearchCriteria ]
    });
  }

  private initSubscriptions() {
    this._valueChangesSub = this.searchCriteria.valueChanges
      .pipe(debounceTime(250), distinctUntilChanged())
      .subscribe(value => this.playerSearchCriteria = value);
  }
  constructor(
    private route:    ActivatedRoute,
    private router:   Router,
    private builder:  FormBuilder,
    private modal:    NzModalService,
  ){}

  handlePlayerSelectedEvent(player: Player) {
    this.router.navigate([player.id], { relativeTo: this.route });
  }

  get searchCriteria() {
    return this._form.get('search');
  }

  get responsiveGutterClasses(): any[] {
    return [
      { xs: 8, sm: 16, md: 24, lg: 24, xl: 24, xxl: 24 },
      { xs: 8, sm: 16, md: 24, lg: 24, xl: 24, xxl: 24 }
    ];
  }

  get form(): FormGroup {
    return this._form;
  }


  handlePlayerAddEvent() {
    this.modal.create<PlayerDialogComponent, PlayerDialogResult>({
      nzCloseIcon: 'close',
      nzContent:    PlayerDialogComponent,
    }).afterClose.subscribe( result => {
      if (result.type == 'success') {
        this.players = [...this.players, result.data];
      }
    })
  }
}
