import {Component, OnDestroy, OnInit} from '@angular/core';
import { PlayerDetails } from "@data/model/player.models";
import {Registration, Statistics} from "@data/model/common.models";
import { Game } from "@data/model/game.models";
import {ActivatedRoute, Router} from "@angular/router";
import {NzModalService} from "ng-zorro-antd/modal";
import {PlayerDialogComponent} from "@shared/widgets/dialogs/player-dialog/player-dialog.component";

@Component({
  selector: 'player-details-view',
  templateUrl: './player-details.component.html',
  styleUrls: ['./player-details.component.less']
})
export class PlayerDetailsComponent implements OnInit, OnDestroy {
  playerDetails:      PlayerDetails;
  playerRegistration: Registration
  recentStats:        Statistics[];
  recentGames:        Game[];

  ngOnInit(): void {
    this.initData();
  }

  ngOnDestroy(): void {}

  private initData() {
    this.playerDetails      = this.route.snapshot.data['playerDetails'][0] as PlayerDetails;
    this.playerRegistration = this.route.snapshot.data['playerDetails'][1] as Registration;
    this.recentGames        = this.route.snapshot.data['playerDetails'][2] as Game[];
    this.recentStats        = this.route.snapshot.data['playerDetails'][3] as Statistics[];
  }

  constructor(private route: ActivatedRoute, private router: Router){}

  handleGameSelectedEvent(selectedGame: Game) {
    this.router.navigate(['games', selectedGame.id], );
  }
}
