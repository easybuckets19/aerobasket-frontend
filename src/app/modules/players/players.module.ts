import { NgModule } from '@angular/core';

import { PlayersRoutingModule } from './players-routing.module';
import { SharedModule } from "@shared/shared.module";
import { PlayerDetailsComponent } from "@modules/players/views/details-view/player-details.component";
import { PlayersComponent } from "@modules/players/views/list-view/players.component";

@NgModule({
  declarations: [
    PlayerDetailsComponent,
    PlayersComponent,
  ],
  imports: [
    PlayersRoutingModule,
    SharedModule,
  ],
})
export class PlayersModule { }
