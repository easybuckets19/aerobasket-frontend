import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import {forkJoin} from "rxjs";
import {PlayerService} from "@data/services/player.service";
import {delay} from "rxjs/operators";

@Injectable()
export class PlayerDetailsResolver implements Resolve<any> {
  constructor(
    private router: Router,
    private playerService: PlayerService,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    const playerDetails$          = this.playerService.getPlayerDetails(this.currentPath(route));
    const registrationDetails$    = this.playerService.getRegistration(this.currentPath(route));
    const recentGames$            = this.playerService.getGames(this.currentPath(route));
    const recentStats$            = this.playerService.getStats(this.currentPath(route));

    return forkJoin([
      playerDetails$,
      registrationDetails$,
      recentGames$,
      recentStats$
    ]);
  }

  private currentPath(route: ActivatedRouteSnapshot): number {
    return Number.parseInt(route.paramMap.get('id'));
  }
}
