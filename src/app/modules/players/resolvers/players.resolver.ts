import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import {PlayerService} from "@data/services/player.service";
import {Observable} from "rxjs";

@Injectable()
export class PlayersResolver implements Resolve<any> {
  constructor(
    private router: Router,
    private playerService: PlayerService,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    return this.playerService.getPlayers();
  }
}
