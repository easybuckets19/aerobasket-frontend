import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlayersComponent } from "@modules/players/views/list-view/players.component";
import { PlayerDetailsComponent } from "@modules/players/views/details-view/player-details.component";
import {PlayersResolver} from "@modules/players/resolvers/players.resolver";
import {PlayerDetailsResolver} from "@modules/players/resolvers/player-details.resolver";
const routes: Routes = [
  {
    path: '',
    component:  PlayersComponent,
    resolve:    {
      players: PlayersResolver
    }
  },
  {
    path: ':id',
    component:  PlayerDetailsComponent,
    resolve:    {
      playerDetails: PlayerDetailsResolver
    }
  },
];

@NgModule({
  imports:    [RouterModule.forChild(routes)],
  exports:    [RouterModule],
  providers:  [PlayerDetailsResolver, PlayersResolver]
})
export class PlayersRoutingModule { }
