import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import {forkJoin} from "rxjs";
import {count, delay} from "rxjs/operators";
import {TeamService} from "@data/services/team.service";
import {PlayerService} from "@data/services/player.service";
import {GameService} from "@data/services/game.service";

@Injectable()
export class OverviewResolver implements Resolve<any> {
  constructor(
    private router: Router,
    private playerService: PlayerService,
    private teamService: TeamService,
    private gameService: GameService,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    const playerRankings$ = this.playerService.getRankings();
    const teamRankings$   = this.teamService.fetchRankings();
    const recentGames$    = this.gameService.fetchRecentGames();
    const upcomingGames$  = this.gameService.fetchUpcomingGames();

    return forkJoin([
      playerRankings$,
      teamRankings$,
      recentGames$,
      upcomingGames$
    ]);
  }

  private currentPath(route: ActivatedRouteSnapshot): number {
    return Number.parseInt(route.paramMap.get('id'));
  }
}
