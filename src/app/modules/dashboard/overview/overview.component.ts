import { Component, OnInit } from '@angular/core';
import {PlayerRanking} from "@data/model/player.models";
import {ActivatedRoute} from "@angular/router";
import {TeamRanking} from "@data/model/team.models";
import {Game} from "@data/model/game.models";

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.less']
})
export class OverviewComponent implements OnInit {
  playerRankings: PlayerRanking[];
  teamRankings: TeamRanking[];
  recentGames: Game[];
  upcomingGames: Game[];
  playersActive:  number;
  teamsActive:  number;

  ngOnInit(): void {
    this.initData();
  }

  ngOnDestroy(): void {}

  constructor(private route: ActivatedRoute){}

  private initData() {
    this.playerRankings   = this.route.snapshot.data['overviewDetails'][0] as PlayerRanking[];
    this.teamRankings     = this.route.snapshot.data['overviewDetails'][1] as TeamRanking[];
    this.recentGames      = this.route.snapshot.data['overviewDetails'][2] as Game[];
    this.upcomingGames    = this.route.snapshot.data['overviewDetails'][3] as Game[];
    this.playersActive    = this.playerRankings.length;
    this.teamsActive      = this.teamRankings.length;
  }

}
