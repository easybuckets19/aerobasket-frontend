import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {OverviewComponent} from "@modules/dashboard/overview/overview.component";
import {OverviewResolver} from "@modules/dashboard/overview/overview.resolver";


const routes: Routes = [
  {
    path: '',
    component:  OverviewComponent,
    resolve: {
      overviewDetails: OverviewResolver
    }
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [OverviewResolver]
})
export class DashboardRoutingModule { }
