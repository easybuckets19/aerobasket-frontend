import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {FormBuilder, FormGroup} from "@angular/forms";
import {debounceTime, distinctUntilChanged} from "rxjs/operators";
import {ActivatedRoute, Router} from "@angular/router";
import {Team} from "@data/model/team.models";
import {PlayerDialogComponent, PlayerDialogResult} from "@shared/widgets/dialogs/player-dialog/player-dialog.component";
import {NzModalService} from "ng-zorro-antd/modal";
import {TeamDialogComponent, TeamDialogResult} from "@shared/widgets/dialogs/team-dialog/team-dialog.component";

@Component({
  selector: 'team-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.less']
})
export class ListViewComponent implements OnInit, OnDestroy {

  private _form:              FormGroup;
  private _valueChangesSub$:  Subscription;

  teams: Team[];
  teamSearchCriteria: string = '';

  ngOnInit(): void {
    this.initData();
    this.initSubscriptions();
  }

  ngOnDestroy(): void {
    this._valueChangesSub$.unsubscribe();
  }

  private initSubscriptions() {
    this._valueChangesSub$ = this.searchCriteria.valueChanges
      .pipe(debounceTime(250), distinctUntilChanged())
      .subscribe(value => this.teamSearchCriteria = value);
  }

  private initData() {
    this.teams = this.route.snapshot.data['teams'];
    this._form = this.builder.group({
      search: [ this.teamSearchCriteria ]
    });
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private builder: FormBuilder,
    private modal:   NzModalService,
  ){}

  handleTeamSelectedEvent(team: Team) {
    this.router.navigate([team.id], { relativeTo: this.route });
  }

  get searchCriteria() {
    return this._form.get('search');
  }

  get responsiveGutterClasses(): any[] {
    return [
      { xs: 8, sm: 16, md: 24, lg: 24, xl: 24, xxl: 24 },
      { xs: 8, sm: 16, md: 24, lg: 24, xl: 24, xxl: 24 }
    ];
  }

  get form(): FormGroup {
    return this._form;
  }

  handleTeamAddEvent() {
    console.log('Hi');
    this.modal.create<TeamDialogComponent, TeamDialogResult>({
      nzCloseIcon: 'close',
      nzContent:    TeamDialogComponent,
    }).afterClose.subscribe( result => {
      if (result.type == 'success') {
        this.teams = [...this.teams, result.data];
      }
    })
  }

}
