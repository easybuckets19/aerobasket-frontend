import {Component, OnDestroy, OnInit} from '@angular/core';
import {Team, TeamDetails} from "@data/model/team.models";
import { Statistics } from "@data/model/common.models";
import { Game } from "@data/model/game.models";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'team-details-view',
  templateUrl: './details-view.component.html',
  styleUrls: ['./details-view.component.less']
})
export class DetailsViewComponent implements OnInit, OnDestroy {
  teamDetails:  TeamDetails;
  recentStats:  Statistics[];
  recentGames:  Game[];

  ngOnInit(): void {
    this.initData();
  }

  ngOnDestroy(): void {}

  private initData() {
    this.teamDetails        = (this.route.snapshot.data['teamDetails'][0] as TeamDetails);
    this.recentGames        = (this.route.snapshot.data['teamDetails'][1] as Game[]);
    this.recentStats        = (this.route.snapshot.data['teamDetails'][2] as Statistics[]);
  }

  constructor(private route: ActivatedRoute, private router: Router){}

  handleGameSelectedEvent(selectedGame: Game) {
    this.router.navigate(['games', selectedGame.id], );
  }

}
