import {Component, Input, OnInit} from '@angular/core';
import {TeamDetails} from "@data/model/team.models";

@Component({
  selector: 'team-details-widget',
  templateUrl: './details-widget.component.html',
  styleUrls: ['./details-widget.component.less']
})
export class DetailsWidgetComponent implements OnInit {
  @Input()  details:      TeamDetails
  @Input()  title:        string;

  constructor() { }

  ngOnInit(): void {
  }

}
