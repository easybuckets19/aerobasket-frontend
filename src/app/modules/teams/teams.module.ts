import { NgModule } from '@angular/core';

import { TeamsRoutingModule } from './teams-routing.module';
import { SharedModule } from "@shared/shared.module";
import { ListViewComponent } from "@modules/teams/views/list-view/list-view.component";
import { DetailsWidgetComponent } from './widgets/details-widget/details-widget.component';
import { DetailsViewComponent } from './views/details-view/details-view.component';

@NgModule({
  declarations: [
    ListViewComponent,

    DetailsWidgetComponent,
    DetailsViewComponent,
  ],
  imports: [
    TeamsRoutingModule,
    SharedModule
  ]
})
export class TeamsModule { }
