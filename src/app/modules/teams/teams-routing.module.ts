import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListViewComponent } from "@modules/teams/views/list-view/list-view.component";
import { TeamsResolver } from "@modules/teams/resolvers/teams.resolver";
import { TeamDetailsResolver } from "@modules/teams/resolvers/team-details.resolver";
import { DetailsViewComponent } from "@modules/teams/views/details-view/details-view.component";

const routes: Routes = [
  {
    path: '',
    component:  ListViewComponent,
    resolve:    {
      teams: TeamsResolver
    }
  },
  {
    path: ':id',
    component:  DetailsViewComponent,
    resolve: {
      teamDetails: TeamDetailsResolver
    }
  },
];

@NgModule({
  imports:    [RouterModule.forChild(routes)],
  exports:    [RouterModule],
  providers:  [TeamDetailsResolver, TeamsResolver]
})
export class TeamsRoutingModule { }
