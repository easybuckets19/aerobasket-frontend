import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import {Observable} from "rxjs";
import {TeamService} from "@data/services/team.service";

@Injectable()
export class TeamsResolver implements Resolve<any> {
  constructor(
    private router: Router,
    private teamService: TeamService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    return this.teamService.fetchTeams();
  }
}
