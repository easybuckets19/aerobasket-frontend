import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import {forkJoin} from "rxjs";
import {delay} from "rxjs/operators";
import {TeamService} from "@data/services/team.service";

@Injectable()
export class TeamDetailsResolver implements Resolve<any> {
  constructor(
    private router: Router,
    private teamService: TeamService,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    const teamDetails$            = this.teamService.fetchTeamDetails(this.currentPath(route));
    const recentGames$            = this.teamService.fetchRecentGames(this.currentPath(route));
    const recentStats$            = this.teamService.fetchRecentStats(this.currentPath(route));

    return forkJoin([
      teamDetails$,
      recentGames$,
      recentStats$
    ]);
  }

  private currentPath(route: ActivatedRouteSnapshot): number {
    return Number.parseInt(route.paramMap.get('id'));
  }
}
