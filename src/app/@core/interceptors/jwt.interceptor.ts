import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import {AuthStoreService} from "@core/services/auth-store.service";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  constructor(private authStore: AuthStoreService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.authStore.getTokenSnapShot();
    if (token && JwtInterceptor.tokenRequired(request.url)) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`
        }
      });
    }
    return next.handle(request);
  }

  private static tokenRequired(url: string): boolean {
    return !(url.includes('/auth/login') || url.includes('/auth/register'));
  }
}
