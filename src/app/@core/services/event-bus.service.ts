import { Injectable } from '@angular/core';
import {Subject, Subscription} from "rxjs";
import {delay, filter, map} from "rxjs/operators";

export type EventType = 'LoginSuccess' | 'LoginExpired';

export class Event {
  constructor(private _type: EventType, private _data?: any) {}

  get type(): EventType {
    return this._type;
  }

  get data(): any {
    return this._data;
  }
}

@Injectable({
  providedIn: 'root'
})
export class EventBusService {
  private subject$ = new Subject<Event>();

  constructor() { }

  public emitEvent(event: Event) {
    this.subject$.next(event);
  }

  public onEvent(eventType: string, action: any, delayed: number): Subscription {
    return this.subject$.pipe(
      delay(delayed),
      filter( (e: Event) => e.type === eventType),
      map( (e: Event) => e["type"]))
    .subscribe(action);
  }
}
