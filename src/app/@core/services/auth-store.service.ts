import { Injectable } from '@angular/core';
import {AuthService} from "@data/services/auth.service";
import {TokenService} from "@core/services/token.service";
import {BehaviorSubject, Observable, of, Subject, Subscription, throwError, timer} from "rxjs";
import {User} from "@data/model/user.model";
import {LoginDTO, RegistrationDTO} from "@data/payload/auth.payloads";
import {catchError, map} from "rxjs/operators";
import {NzMessageService} from "ng-zorro-antd/message";
import jwtDecode from "jwt-decode";
import {EventBusService, Event} from "@core/services/event-bus.service";

@Injectable({
  providedIn: 'root'
})
export class AuthStoreService {
  private currentUser  = new BehaviorSubject<User>(null);
  private currentToken = new BehaviorSubject<string>(null);

  public user$: Observable<User> = this.currentUser.asObservable();
  public token$: Observable<string> = this.currentToken.asObservable();

  private timerSub: Subscription;

  constructor(
    private eventBusService: EventBusService,
    private authService: AuthService,
    private tokenService: TokenService,
    private messageService: NzMessageService,
  ) {
    const storageToken = this.tokenService.getAccessToken();
    const storageUser = this.tokenService.getUser();
    if(storageToken && storageUser) {
     const decodedToken = jwtDecode(storageToken);
     this.syncAuth(storageToken, storageUser);
     this.initTimerSub(decodedToken);
    }
  }

  public login(credentials: LoginDTO): Observable<User> {
    return this.authService.login(credentials)
      .pipe(
        map( authResult => {
            const { accessToken, ...accessUser } = authResult;
            this.syncStorage(accessToken, accessUser);
            this.syncAuth(accessToken, accessUser);
            this.messageService.success('Login Successful!');
            this.eventBusService.emitEvent(new Event('LoginSuccess'))
            return authResult;
          }),
        catchError(error => {
          this.messageService.error(error.error.message);
          return throwError(error);
        })
      )
  }

  public register(credentials: RegistrationDTO): Observable<User> {
    return this.authService.register(credentials)
      .pipe(
        map( authResult => {
          this.messageService.success('Registration successful');
          return authResult;
        }),
        catchError(error => {
          this.messageService.error(error.error.message);
          return throwError(error)
        })
      );
  }

  public logout(): void {
    this.resetAuth();
  }

  public getUserSnapshot(): User {
    return this.currentUser.value;
  }

  public getTokenSnapShot(): string {
    return this.currentToken.value;
  }

  private syncStorage(token: string, user: User) {
    this.tokenService.saveAccessToken(token);
    this.tokenService.saveUser(user);
  }

  private syncAuth(storageToken: string, storageUser: User): void {
    this.currentUser.next(storageUser);
    this.currentToken.next(storageToken);
  }

  private resetAuth(): void {
    this.tokenService.purge();
    this.currentUser.next(null);
    this.currentToken.next("");
  }

  private initTimerSub(decodedToken: any) {
    this.timerSub = timer(
      new Date(decodedToken.exp * 1000)
    )
    .subscribe( () => {
      this.eventBusService.emitEvent(new Event('LoginExpired'));
      this.resetAuth();
    });
  }
}
