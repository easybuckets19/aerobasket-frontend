import { Injectable } from '@angular/core';
import {User} from "@data/model/user.model";

const TOKEN_KEY = 'auth_token';
const USER_KEY = 'auth_user';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor() { }

  public purge(): void {
    window.localStorage.clear();
  }

  public saveAccessToken(token: string): void {
    window.localStorage.setItem(TOKEN_KEY, token);
  }

  public saveUser(user: any): void {
    window.localStorage.setItem(USER_KEY, JSON.stringify(user));
  }

  public getAccessToken(): string | null {
    return window.localStorage.getItem(TOKEN_KEY);
  }

  public getUser(): User | null {
    const user = window.localStorage.getItem(USER_KEY);
    return JSON.parse(user);
  }

}
