import {Component, OnDestroy, OnInit} from '@angular/core';
import {EventBusService} from "@core/services/event-bus.service";
import {Router} from "@angular/router";
import {Subscription} from "rxjs";
import {NzMessageService} from "ng-zorro-antd/message";
import {delay} from "rxjs/operators";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnDestroy, OnInit {
  isCollapsed = false;
  eventBusActions: Subscription = new Subscription();

  constructor(
    private eventBus: EventBusService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.eventBusActions.add(
      this.eventBus.onEvent(
        'LoginSuccess',
        () => this.router.navigate(['/dashboard']),
        2500
      ));

    this.eventBusActions.add(
      this.eventBus.onEvent(
        'LoginExpired',
        () => this.router.navigate(['/auth']),
        0
      )
    );
  }

  ngOnDestroy(): void {
    this.eventBusActions.unsubscribe()
  }
}
