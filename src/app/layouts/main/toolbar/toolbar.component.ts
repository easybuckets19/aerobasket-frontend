import {Component, Input, OnInit} from '@angular/core';
import {User} from "@data/model/user.model";


@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.less']
})
export class ToolbarComponent implements OnInit {
  @Input() toolbarUser: User;


  constructor() { }

  ngOnInit(): void {
  }

}
