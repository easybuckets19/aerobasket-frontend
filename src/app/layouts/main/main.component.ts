import {Component, Input, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {User} from "@data/model/user.model";
import {AuthStoreService} from "@core/services/auth-store.service";
@Component({
  selector: 'app-main-layout',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.less']
})
export class MainComponent implements OnInit {
  appUser$: Observable<User>;
  appCollapsed: boolean = true;

  constructor(private authStore: AuthStoreService) {
    this.appUser$ = this.authStore.user$;
  }

  ngOnInit(): void {
  }
}


