import {Component, Input, OnInit} from '@angular/core';
import {AntGroupItem} from "../../../@shared/ant-design/components/group-item/group-item.types";

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.less']
})
export class SidebarComponent implements OnInit {
  @Input() sidebarItems: AntGroupItem[];
  @Input() sidebarCollapsed: boolean;

  constructor() {}

  ngOnInit(): void {
    this.sidebarItems = [
      {
        /*  ------------------ A. Module - Lazy loaded [w. additional paths] ------------------  */
        title: 'General',
        items: [
          // 1. Route - Overview [w. routerLink)
          {
            label:            'Dashboard',
            icon:             'antx:dashboard',
            routerLink:       '/dashboard'
          },
          // 2. Route - Players [w. routerLink)
          {
            label:            'Players',
            icon:             'antx:players',
            routerLink:       '/players'
          },
          // 3. Route - Teams [w. routerLink)
          {
            label:            'Teams',
            icon:             'antx:teams',
            routerLink:       '/teams'
          },
          // 4. Route - Games [w. routerLink)
          {
            label:            'Games',
            icon:             'antx:games',
            routerLink:       '/games'
          },
        ],
      },

    ]
  }

}
