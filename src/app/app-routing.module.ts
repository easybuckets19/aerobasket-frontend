import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from "./layouts/main/main.component";
import { AuthComponent } from "./layouts/auth/auth.component";
import {AuthGuard} from "@core/guards/auth.guard";
import {RedirectComponent} from "./layouts/redirect/redirect.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: '/auth/login',
    pathMatch: 'full'
  },
  {
    path: '',
    component: MainComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        loadChildren: () =>
          import('@modules/dashboard/dashboard.module')
            .then(m => m.DashboardModule)
      },
      {
        path: 'players',
        loadChildren: () =>
          import('@modules/players/players.module')
            .then(m => m.PlayersModule)
      },
      {
        path: 'teams',
        loadChildren: () =>
          import('@modules/teams/teams.module')
            .then(m => m.TeamsModule)
      },
      {
        path: 'games',
        loadChildren: () =>
          import('@modules/games/games.module')
            .then(m => m.GamesModule)
      },
    ]
  },
  {
    path: 'auth',
    component: AuthComponent,
    loadChildren: () =>
      import('@modules/auth/auth.module')
        .then(m => m.AuthModule)
  },
  // Fallback when no prior routes is matched
  { path: 'unauthorized', component: RedirectComponent },
  // { path: '**', redirectTo: '/auth/login', pathMatch: 'full' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
